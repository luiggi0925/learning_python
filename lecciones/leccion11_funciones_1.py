#########################
#  Funciones - Parte 1  #
#########################

# Las funciones permiten reutilizar código. Python provee funciones para
# interactuar con los tipos de datos que nos provee. Nosotros podemos crear
# nuestras propias funciones para lógica o sentencias personalizadas.

print('Definición de funciones:')

# Las funciones se definen:
#
# def <nombre_funcion>(<argumento 1>, <argumento 2>, ...):
#     <sentencias...>
#     return <value>
#
# Las funciones pueden tener 0, 1 o muchos argumentos. Pueden devolver 0 o 1
# resultados. Para devolver un valor, se usa `return`.
# Cuando se ejecuta `return`, se detiene la ejecución de la función.

# Aquí se define la función `say_hello()`.
def say_hello():
    print('¡Hola!')

# Podemos usar esta función múltiples veces.
say_hello()
say_hello()
# Una función es un objeto de tipo 'function'. Revisaremos esto en detalle más
# adelante.
print(type(say_hello)) # <class 'function'>

# Nuestra función puede tener más de 1 sentencia.
def say_hello_with_weather():
    print('¡Hola!')
    print('Hace buen clima hoy.')

say_hello_with_weather()

# Función para sumar 2 números
def sum(num1, num2):
    return num1 + num2

print(sum(2, 3)) # 5
print(sum(100, -27)) # 73

# Función para capitalizar todas las oraciones contenidas en una cadena.
# Para facilitar esta función, las oraciones solo estarán divididas por '.'
def capitalize_sentences(paragraph):
    sentences = paragraph.split('.')
    result = []
    for sentence in sentences:
        result.append(sentence.strip().capitalize())
    return ". ".join(result)

print(capitalize_sentences('aprendo python. creo funciones. y me gusta.')) # Aprendo python. Creo funciones. Y me gusta.

print('---------------------------------------------------------------------')
print('Tipos de datos de argumentos y retorno:')

# Para facilitar nuestra experiencia y comunicar mejor las intenciones de la
# función, podemos colocar los tipos de datos esperados para los argumentos y
# el tipo de dato de salida.

# Indicamos que esta función debe recibir un `float` y devolverá un `float`.
def apply_taxes(amount: float) -> float:
    return amount * 1.21

print(apply_taxes(100)) # 121.0

# En la lección anterior escribimos el cálculo de los primeros 10 términos de la
# serie Fibonacci. Ahora usaremos ese código para crear una función que retorne
# el `n`-ésimo término. `n` será un argumento de nuestra función.
def fibonacci(n: int) -> int:
    a, b, count = 0, 1, 2
    while count < n:
        count += 1
        a, b = b, b + a
    return b

print(fibonacci(5)) # 3
print(fibonacci(10)) # 34

# Se usa el tipo `None` para indicar que nuestra función no devuelve resultado.
def greet(name: str) -> None:
    print(f'¡Hola {name}!')

greet('Luiggi') # '¡Hola Luiggi!'

print('---------------------------------------------------------------------')
print('Valores por defecto de argumentos:')

# Si invocamos una función sin colocar los argumentos solicitados, se lanza un
# error.
# greet() # TypeError: greet() missing 1 required positional argument: 'name'

# Podemos usar valores por defecto para aquellas variables que no sean
# obligatorias.
def greet_anyone(name: str = 'guapo desconocido') -> None:
    print(f'¡Hola {name}!')

greet_anyone('Bob Esponja') # '¡Hola Bob Esponja!'
# Invocar esta función sin argumento no generará error.
greet_anyone() # '¡Hola guapo desconocido!'

# Impresión de un cuadrado. Por defecto, se imprimirá un cuadrado de longitud 8.
def print_square(n: int = 8) -> None:
    horizontal = ''
    for i in range(n):
        horizontal += '* '
    print(horizontal)
    for i in range(n-2):
        line = '* '
        for j in range(n-2):
            line += '  '
        line += '*'
        print(line)
    print(horizontal)

print_square()
print_square(5)

print('---------------------------------------------------------------------')
print('Argumentos por nombre:')

# Algunas funciones pueden poseer múltiples argumentos. Puede que debamos
# editarlas y agreguemos algún argumento. Esto podría ocasionar que los usuarios
# de esta función dejen de funcionar.
# Una alternativa es agregar las variables al final. Pero Python también nos
# provee un mecanismo de indicar los argumentos por nombre.

# Tip: los argumentos con valores requeridos deben ir primero, luego los
# argumento con valores por defecto.
def footer(company_name: str, year: int=2023, trademark:bool = True):
    tm = '™' if trademark else ''
    return f'Copyright {company_name.capitalize()}{tm}. Year {year}.'

# Ejecutamos `footer()` con valores en desorden "por error".
print(footer('Acme', False, 2020)) # Copyright Acme™. Year False. (ugh)

# Esto se puede resolver usando los argumentos por nombre
acme_footer = footer('Acme', trademark=False, year=2020)
print(acme_footer) # Copyright Acme Year 2020.

# Al usar este mecanismo, debemos tener cuidado con los siguientes casos:

# 1. Omitir los argumentos requeridos
# print(footer()) # TypeError: footer() missing 1 required positional argument: 'company_name'

# 2. Luego de colocar un argumento por nombre, todos los demás deben ir por
#    nombre
# print(footer(company_name='Acme', 2020)) # SyntaxError: positional argument follows keyword argument

# 3. Evitar duplicar el valor de un argumento.
# print(footer('Acme', company_name='Acme')) # TypeError: footer() got multiple values for argument 'company_name'

# 4. Usar un nombre de argumento desconocido.
# print(footer('Acme', media='Twitter_link')) # TypeError: footer() got an unexpected keyword argument 'media'

print('---------------------------------------------------------------------')
print('Cantidad desconocida de argumentos:')

# Podemos crear funciones donde la cantidad de argumentos sea dinámica. Un
# ejemplo es la función `print()` que hemos usado desde la primera lección.
print() # Salto de línea
print('Hola', 'mundo') # 'Hola mundo'
print([1,2,3], (4,5,6), {7,8,9}, {10:10, 11:11, 12:12}) # [1, 2, 3] (4, 5, 6) {8, 9, 7} {10: 10, 11: 11, 12: 12}

# Nosotros podemos hacerlo declarando un argumento con `*` como prefijo.
def my_basic_print(*args):
    print(args)

my_basic_print() # ()
my_basic_print('Hola', 'mundo') # ('Hola', 'mundo')

# Detrás de las escenas, la variable `*args` es una tupla. Eso significa que 
# podemos acceder a sus elementos por índice.

def another_greet(*args):
    print(args + ("!",) if args and args[0] == 'Hola' else args)

another_greet() # ()
another_greet('Hola', 'Luiggi') # ('Hola', 'Luiggi', '!')

print('---------------------------------------------------------------------')
print('Colecciones como argumentos:')

# Podemos usar colecciones como argumentos de nuestras funciones.

def print_list(l) -> None:
    for item in l:
        print(item)

print_list([10, 12, 13]) # 10 12 13
print_list([10, 12, 13]) # 10 12 13

# Hay que tener cuidado cuando usemos colecciones con argumentos por defecto.
# Ejemplo:

def add_number(l = [], n: int = 0):
    l.append(n)
    return l

# Probando con una lista como argumento.
my_list = [1, 2]
add_number(my_list, 3)
print(my_list) # [1, 2, 3]
add_number(my_list)
print(my_list) # [1, 2, 3, 0]
# Probando con la lista vacía como argumento por defecto.
l = add_number()
print(l) # [0]
l = add_number()
print(l) # [0, 0] ?!?!
print(my_list) # [1, 2, 3, 0] Esta no cambió.

# ¿Qué sucedió? ¿Por qué devolvió una lista con dos elementos en lugar de 1?
# El problema está en el valor por defecto: `l = []`. Sucede que los valores por
# defecto son evaluados solo 1 vez por el intérprete de Python, y luego son
# reutilizados. Esto significa que la lista vacía usada como valor por defecto
# será reutilizada en múltiples ejecuciones de la función cuando no indiquemos
# un argumento.
#
# Para evitar esta situación, debemos cambiar el argumento por defecto a `None`
# y asignar el valor por defecto dentro de la función.

def fixed_add_number(l = None, n: int = 0):
    l = l if l else []
    l.append(n)
    return l

l = fixed_add_number()
print(l)
l = fixed_add_number()
print(l)
