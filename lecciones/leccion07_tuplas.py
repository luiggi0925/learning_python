##################################
#  Estructuras de datos: Tuplas  #
##################################

# La tupla es una estructura de datos que permite almacenar una cantidad
# conocida de elementos. Almacena los elementos de manera contigua y permite el
# acceso por índice. Esta es una estructura de datos inmutable. Esto significa
# que no permite nuevos elementos, no permite reemplazar elementos existentes y
# tampoco permite remover elementos de ella.
# Si necesitamos modificar los elementos de una tupla, en su lugar tendremos que
# crear una nueva tupla con los nuevos datos requeridos.

# Las tuplas sirven como un mecanismo de almacenamiento de datos que se entrelazan
# para un propósito común. Ejemplos:
# - Coordenadas (latitud, longitud) de un punto de un mapa.
# - Datos de una persona: nombre, fecha de nacimiento, correo electrónico.
# - Representación de los lados de una figura geométrica.
#
# Características de las tuplas:
# - Conocimiento de datos al momento de creación.
# - Acceso por índice. Los índices son numéricos y van de 0 a `len(tupla) - 1`.
# - No permite agregar datos.
# - No permite actualizar datos.
# - No permite eliminar datos.
# - Permite elementos duplicados.
#
# Análisis asintótico de las operaciones con Big O:
#   Operación    Mejor   Promedio   Peor
# -----------------------------------------
#   Inserción:     -        -        -
#   Acceso:       O(1)     O(1)     O(1)
#   Búsqueda:     O(1)     O(N)     O(N)
#   Eliminación:   -        -        -
#

print('Creación de tupla:')
# Una tupla se inicializa con paréntesis `()`
my_tuple = () # Tupla vacía.
print(my_tuple) # ()
print(type(my_tuple)) # <class 'tuple'>

my_tuple = (1, 2, 3) # Tupla con datos.
print(my_tuple) # (1, 2, 3)

my_tuple = ('Luiggi', 19, 'Perú', 'España')
print(my_tuple) # ('Luiggi', 19, 'Perú', 'España')

print('---------------------------------------------------------------------')
print('Acceso a elementos:')

# Similar a las listas, las tuplas permiten acceder a los elementos mediante
# el índice.

name = my_tuple[0]
print(name) # 'Luiggi'
age = my_tuple[1]
print(age) # 19

# También podemos acceder usando índices negativos.
current_country = my_tuple[-1]
print(current_country) # 'España'

# La función `len()` devolverá el tamaño de la tupla.
print(len(my_tuple)) # 4

# Al usar un índice fuera de los límites, se lanzará un error.
# my_tuple[5] # IndexError: tuple index out of range

# Podemos desempaquetar el contenido de la tupla en múltiples variables.
name, age, birth_country, current_country = my_tuple
print(f'name={name} age={age} birth_country={birth_country} current_country={current_country}') # name=Luiggi age=19 birth_country=Perú current_country=España

# Usar menos variables que el contenido de la tupla lanzará un error.
# name, age = my_tuple # ValueError: too many values to unpack (expected 2)

# También se lanzará un error cuando se intente desempaquetar en más variables
# que el contenido.
# a, b, c, d, e, f, g = my_tuple # ValueError: not enough values to unpack (expected 7, got 4)

print('---------------------------------------------------------------------')
print('Búsqueda de elementos:')

# La tupla tiene las funciones `index()` y `count()` que funcionan similar a
# las de la lista.
print(my_tuple.index('Luiggi')) # 0
print(my_tuple.index('Perú')) # 2

# `index()` arrojará un error al buscar un elemento que no existe.
# print(my_tuple.index(10)) # ValueError: tuple.index(x): x not in tuple
print(my_tuple.count('Luiggi')) # 1
print(my_tuple.count(19)) # 1
print(my_tuple.count(10)) # 0

print('---------------------------------------------------------------------')
print('Concatenación de tuplas:')

# Debido a que las tuplas no se pueden modificar, para unir dos tuplas se
# creará una nueva tupla con el contenido de las dos tuplas a unir.
first_tuple = (1, 5, 20)
second_tuple = (10, 12, 15)
result_tuple = first_tuple + second_tuple
print(result_tuple) # (1, 5, 20, 10, 12, 15)

# Operaciones como reemplazar un valor o usar `del` lanzarán error.
# my_tuple[0] = 10 # TypeError: 'tuple' object does not support item assignment
# del my_tuple[1] # TypeError: 'tuple' object doesn't support item deletion

print('---------------------------------------------------------------------')
print('Conversión a lista:')

# Se puede convertir una tupla a lista usando `list()`.
my_list = list(my_tuple)
print(my_list) # ['Luiggi', 19, 'Perú', 'España']

# También se puede convertir una lista a tupla usando `tuple()`.
another_tuple = tuple(my_list)
print(another_tuple) # ('Luiggi', 19, 'Perú', 'España')
