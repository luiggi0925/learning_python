##############################
#  Aplicaciones multi hilos  #
##############################

#

from threading import Thread, Event, current_thread, Lock
import time
from typing import Any, Callable, Iterable, Mapping

print('Multi threading básico:')

# Existen 2 mecanismos para declarar funcionalidades que se ejecutarán en otro
# hilo.
# Mecanismo 1: crear una clase que extienda de `threading.Thread`.
# Ejecutaremos 2 tareas que imprimirán números del 1 al 10 usando multi hilos.
class MyThread(Thread):
    # Debemos sobreescribir la función `run()`
    def run(self):
        for i in range(1,11):
            print(self.name, i)

# Creamos nuestros objetos de nuestra clase particular.
thread1 = MyThread(name='H1')
thread2 = MyThread(name='H2')

print('Ejecutando hilos desde subclase:')
# Para iniciar la ejecución del hilo, debemos ejecutar la función `start()`.
print('A punto de ejecutar los hilos.')
thread1.start()
thread2.start()
print('Se ejecutan los hilos.')
# Esperamos que los hilos terminen su ejecución con la función `join()`.
thread1.join()
thread2.join()
print('Los hilos terminaron su ejecución.')

# Mecanismo 2: crear una función sin dato de retorno. Crear una instancia de
# `threading.Thread` indicando nuestra función como argumento de nombre
# `target`.

def multiple_threads_run_this():
    for i in range(1,11):
        # Usaremos `threading.current_thread` para acceder al hilo actual.
        print(current_thread().name, i)

# Creamos los objetos `threading.Thread`.
thread1 = Thread(name='H1', target=multiple_threads_run_this)
thread2 = Thread(name='H2', target=multiple_threads_run_this)

print('+++++++++++++++++++++++++++++++')
print('Ejecutando hilos desde función:')
# Para iniciar la ejecución del hilo, debemos ejecutar la función `start()`.
print('A punto de ejecutar los hilos.')
thread1.start()
thread2.start()
print('Se ejecutan los hilos.')
# Esperamos que los hilos terminen su ejecución con la función `join()`.
thread1.join()
thread2.join()
print('Los hilos terminaron su ejecución.')

# También podemos enviar argumentos a nuestra función del hilo mediante el
# argumento `args`.

def thread_function_with_args(stop_at: int):
    for i in range(1, stop_at + 1):
        print(current_thread().name, i)

# Creamos los objetos `threading.Thread`.
thread1 = Thread(name='H1', target=thread_function_with_args, args=[10])
stop_at = 10
thread2 = Thread(name='H2', target=thread_function_with_args, args=[stop_at])
print('+++++++++++++++++++++++++++++++++++++++++++++')
print('Ejecutando hilos desde función con argumento:')
# Para iniciar la ejecución del hilo, debemos ejecutar la función `start()`.
print('A punto de ejecutar los hilos.')
thread1.start()
thread2.start()
print('Se ejecutan los hilos.')
# Esperamos que los hilos terminen su ejecución con la función `join()`.
thread1.join()
thread2.join()
print('Los hilos terminaron su ejecución.')

# La ejecución de esta sección del programa siempre cambiará su resultado. Esto
# sucede porque los hilos son impredecibles.


print('---------------------------------------------------------------------')
print('Bloqueo de hilos:')

# Existen ocasiones en que queremos pausar la ejecución de un hilo. Podemos
# cometer un error tratando de implementar esta pausa de manera inocente.
#
# Comenté este código porque golpea mi CPU, ligeramente pero no quiero
# afectarla.
'''
class StartingAndStoppingPointThread(Thread):
    def __init__(self, name: str | None = None, **kwargs) -> None:
        super().__init__(name=name, kwargs=kwargs)
        self.start_at = kwargs.get('start_at', 1)
        self.stop_at = kwargs.get('stop_at', 11)
        self.paused = False
    def run(self) -> None:
        i: int = self.start_at
        while i < self.stop_at:
            # Esta es una manera ineficiente de pausar un hilo. Solo está para
            # demostración. Por favor no hacer esto en código real porque esto
            # desperdicia CPU. Recomiendo usar una herramienta de monitoreo
            # gráfica del sistema operativo para comprobar cómo aumenta el
            # consumo de CPU desmesuradamente.
            if self.paused: continue
            print(f'{self.name} {i}')
            i += 1
    def pause(self) -> None:
        self.paused = True
    def resume(self) -> None:
        self.paused = False

startMe = StartingAndStoppingPointThread(name='H3', stop_at=100)
startMe.start()
# Pausamos la ejecución.
startMe.pause()
print('Te puse en pausa')
# Esperamos 2.5 segundos para continuar.
# Para comprobar que esta pausa es poco efectiva, aumenta el tiempo de 2.5 a 6
# o más segundos. Escucharás cómo tu CPU comienza a sonar fuerte y a calentarse.
time.sleep(2.5)
print('Ahora podrás continuar')
startMe.resume()
startMe.join()
'''

class ProperPauseAndResumeThread(Thread):
    def __init__(self, event: Event, name: str = None, **kwargs) -> None:
        super().__init__(name=name, kwargs=kwargs)
        self.event = event
        self.paused = False
        self.start_at = kwargs.get('start_at', 1)
        self.stop_at = kwargs.get('stop_at', 101)
    def run(self) -> None:
        i: int = self.start_at
        while i < self.stop_at:
            # Este es un mejor mecanismo para pausar la ejecución de un hilo.
            if self.paused: self.event.wait()
            print(f'{self.name} {i}')
            i += 1
    def pause(self) -> None:
        self.paused = True
    def resume(self) -> None:
        self.paused = False
        self.event.clear()

print('+++++++++++++++++++++++++++++++')
event = Event()
pauseMe = ProperPauseAndResumeThread(event, name='H4')
pauseMe.start()
# Pausamos la ejecución.
pauseMe.pause()
print('Te puse en pausa')
# Esperamos 2.5 segundos para continuar.
# Puedes aumentar el tiempo a lo que quieras. La CPU no se consumirá.
time.sleep(2.5)
print('Ahora podrás continuar')
event.set()
pauseMe.resume()
pauseMe.join()


print('---------------------------------------------------------------------')
print('Estado compartido entre hilos:')

# Existen ocasiones en que los datos deben ser compartidos entre múltiples
# hilos. Puesto que los hilos por naturaleza no tienen un comportamiento que se
# pueda determinar, compartir variables entre ellos resulta una tarea compleja.
# Mal manejado, esto puede llevar a una condición de carrera (race condition),
# lo cual conlleva con resultados inesperados.
# 
# Ejemplo de condición de carrera:

# Creamos una clase cuyo estado será compartido entre múltiples hilos.
class ViewCounter:
    def __init__(self) -> None:
        self._counter = 0
    def count(self) -> None:
        self._counter += 10
    def decrease(self) -> None:
        self._counter -= 10

# Creamos nuestra clase que se ejecutará en otro hilo.
class CounterThread(Thread):
    def __init__(self, view_counter: ViewCounter, times: int = 100000, \
        op: str = '+') -> None:
        super().__init__()
        self._view_counter = view_counter
        self._times = times
        self._op = op
    def run(self):
        for i in range(1, self._times + 1):
            if self._op == '+': self._view_counter.count()
            if self._op == '-': self._view_counter.decrease()

# Creamos el objeto que servirá de estado compartido (objeto en común entre los
# hilos).
commonCounter = ViewCounter()
# Creamos los 2 hilos con el estado compartido.
thread1 = CounterThread(commonCounter)
thread2 = CounterThread(commonCounter, op='-')
thread1.start()
thread2.start()
thread1.join()
thread2.join()

# Si se imprime 0 sin importar las veces que ejecutes el programa, te sugiero
# usar un intérprete con menos recursos como https://www.online-python.com/
# para ejecutar el código de las líneas 183 a 202.
print('Resultado de race condition:', commonCounter._counter) # ¿Será 0?

# La solucion a este caso es usar un candado (lock). El lock es un mecanismo
# para indicar que solo un hilo a la vez podrá ejecutar un bloque de código. El
# hilo en cuestión será encargado de activar y desactivar el lock
# apropiadamente.
#
# Apliquemos el lock en nuestro caso anterior.

class LockedCounterThread(Thread):
    def __init__(self, view_counter: ViewCounter, lock: Lock, \
        times: int = 100000, op: str = '+') -> None:
        super().__init__()
        self._view_counter = view_counter
        self._lock = lock
        self._times = times
        self._op = op
    def run(self):
        for i in range(1, self._times + 1):
            if self._op == '+': 
                self._lock.acquire()
                self._view_counter.count()
                self._lock.release()
            if self._op == '-':
                self._lock.acquire()
                self._view_counter.decrease()
                self._lock.release()

# Creamos el objeto que servirá de estado compartido (objeto en común entre los
# hilos).
commonCounter = ViewCounter()
lock = Lock()
# Creamos los 2 hilos con el estado compartido.
thread1 = LockedCounterThread(commonCounter, lock)
thread2 = LockedCounterThread(commonCounter, lock, op='-')
thread1.start()
thread2.start()
thread1.join()
thread2.join()
# El resultado siempre será 0. Pero en su lugar hemos degradado el rendimiento
# de la aplicación.
print('Resultado de uso de locks:', commonCounter._counter)

