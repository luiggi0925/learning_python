###########################################################
#  Flujos de control: for, while. Iteradores e iterables  #
###########################################################

# Existen ocasiones que queremos repetir una tarea por una cantidad específica
# de veces, o queremos ejecutar cierta lógica con algunos o todos los elementos
# de una lista. Una forma de lograrlo es con bucles (loops en inglés).
#
# Python soporta 2 mecanismos de bucles: `for` y `while`.

print('Bucles for:')
# Estos bucles sirven para recorrer los elementos de un iterable.
# Un iterable es un tipo de dato que permite recorrer los elementos que contiene
# bajo un orden establecido.
# Las listas y las tuplas son iterables desde el primer hasta el último elemento.
# Existen estructuras de datos que ofrecen otros tipos de iteraciones, pero
# posiblemente lo veamos en otra lección.

# Los ejemplos a continuación se harán sobre listas. Se pueden hacer con tuplas.
my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
# Recorrer los elementos de una lista e imprimirlos.
for n in my_list:
    print(n) # Se imprimen uno a uno los elementos de `my_list`.
# Recorrer la lista e imprimir los números pares.
for n in my_list:
    if n % 2 == 0:
        print(n)

names = ['Jorge', 'Ana', 'Pablo', 'Karina']
# Imprimir los nombres y sus tamaños.
for name in names:
    print(f'{name} {len(name)}')

# Imprimir los nombres que poseen la letra 'a'.
for name in names:
    if name.find('a') >= 0:
        print(name)

# Contar cuántos nombres poseen la letra 'r'.
count = 0
for name in names:
    if name.find('r') >= 0:
        count += 1
print(count) # 2

# Ejemplos de iteración sobre diccionario y conjunto.
person = { 'edad': 15, 'nombre': 'Franco', 'apellido': 'Meza' }
for keyval in person.items():
    print(keyval)

students = { 'Maria', 'Juan', 'Roberto', 'Elsa', 'Andrea' }
for student in students:
    print(student)

print('---------------------------------------------------------------------')
print('Función `range()`:')

# Si queremos repetir una tarea una cantidad de veces limitada, o si queremos
# generar números de forma secuencial, podemos usar la función `range()`.

# `range()` recibe por defecto 1 argumento que es el número de Fin (exclusivo).
# Generará valores desde 0 hasta el `Fin - 1`.
for i in range(4):
    print(i) # 0, 1, 2, 3

# También podemos colocar el número de Inicio (inclusivo) y Fin (exclusivo).
for i in range(6, 10):
    print(i) # 6, 7, 8, 9

# Como tercer argumento, se puede colocar el Paso.
for i in range(6, 13, 3):
    print(i) # 6, 9, 12

# También soporta valores negativos
for i in range(4, 2, -1):
    print(i) # 4, 3

# Los números generados por un rango se pueden almacenar en una lista.
pairs = list(range(0, 11, 2))
print(pairs) # [0, 2, 4, 6, 8, 10]

# Se puede usar un rango como estructura auxiliar para iterar la lista.
for i in range(len(names)):
    print(f'Paciente {names[i]} tiene el ticket {i+1}')

# El resultado de `range()` no es una estructura de datos como una lista o un
# set. Es un `range`, que es un generador de datos. La ventaja de usar un
# generador es que no generará ningún valor en su declaración. Solo generará
# los datos cuando se soliciten.

# Esta declaración no genera ningún número.
range_0_100000000000000000000 = range(100000000000000000000) # Es un número grande
# Podemos evaluar el tipo de dato.
print(type(range_0_100000000000000000000)) # <class 'range'>
# Cuando ejecutemos un ciclo `for` sobre el rango empezará a generar datos.
# Esto es una ventaja en rendimiento.

print('---------------------------------------------------------------------')
print('Iterables:')

# Si consumimos el rango en un ciclo for, dependiendo de las características de
# nuestra computadora, es posible que la aplicación se paralice por un momento.
# (Comentado para evitar problemas. Si lo pruebas, puedes detener la aplicación
# usando CTRL + C).
# for i in range_0_100000000000000000000:
#     print(i)

# En su lugar, podemos usar un iterador que nos permite consultar los datos de
# manera controlada.

long_range_iter = iter(range_0_100000000000000000000)
print(type(long_range_iter)) # <class 'longrange_iterator'>

# El iterador se sitúa antes del primer valor a revisar. La función `next()` nos
# permite obtener el siguiente valor presente en el iterador.
# Cada invocación de `next()` solo obtendrá 1 valor del iterador. No calculará
# todos los valores.

print(next(long_range_iter)) # 0
print(next(long_range_iter)) # 1
print(next(long_range_iter)) # 2
print(next(long_range_iter)) # 3

# Cabe resaltar que `next()` solo puede "avanzar" por el iterador. Dependiendo
# de la implementación del iterador, no se podrá obtener el mismo elemento 2
# veces. Todo elemento que fue consumido por `next()` no será evaluado
# nuevamente.

# También podemos obtener iteradores de nuestras estructuras de datos.
name_iter = iter(names)
print(type(name_iter)) # <class 'list_iterator'>
print(next(name_iter)) # 'Jorge'
print(next(name_iter)) # 'Ana'
print(next(name_iter)) # 'Pablo'
print(next(name_iter)) # 'Karina'

# Si no existen más elementos disponibles en el iterador, se lanzará un error.
# print(next(name_iter)) # StopIteration
# En su lugar, se puede pasar un segundo argumento que será el valor por defecto
# a obtener en caso que el iterador haya sido consumido totalmente.
print(next(name_iter, 'Nadie')) # 'Nadie'
print(next(name_iter, None)) # None

# Un iterable es todo aquel objeto que permite ser iterado. Todas las
# estructuras de datos vistas (listas, tuplas, diccionarios, conjuntos) son
# iterables. Los rangos también son iterables.

# Los números no son iterables. Crear un iterador de ellos lanza un error.
# iter(10) # TypeError: 'int' object is not iterable

# Los `bool` tampoco.
# iter(True) # TypeError: 'bool' object is not iterable

# Las cadenas sí son iterables.
print(type(iter('hola mundo'))) # <class 'str_iterator'>

# Detrás de las escenas, todos los ciclos `for` actúan sobre iterables.

for caracter in 'hola mundo':
    print(caracter.upper())

# En una futura lección crearemos nuestros propios iterables.

print('---------------------------------------------------------------------')
print('Bucles while:')

# Otra forma de ejecutar bloques de código múltiples veces es con un bucle
# `while`. A diferencia de `for` que depende de un iterable, `while` depende
# de una serie de condiciones.

count = 1
while count <= 5:
    print(count)
    count += 1 # Esta línea es importante para que se cumpla la condición.

hello = 'hola'
i = 0
while hello[i] != 'l':
    print(hello[i])
    i += 1 # Esta línea es importante para que se cumpla la condición.

# Si la condición de `while` no se satisface, podemos crear un bucle infinito.
# Descomentar para comprobarlo.
# while True:
#     print('Por favor termina la aplicación con CTRL + C!')

# Ejemplo: imprimir los primeros 10 números de la serie Fibonacci:
a = 0
b = 1
count = 2
print('Serie Fibonacci:')
print(a)
print(b)
while count < 10:
    next_fib = a + b
    print(next_fib)
    count += 1
    a = b
    b = next_fib

print('---------------------------------------------------------------------')
print('`break`, `continue`, `else`:')

# Existen ocasiones en que necesitamos cortar el bucle por alguna condición
# particular. En el caso de `for` es porque encontramos el elemento esperado; en
# el caso de `while` porque estamos en un bucle infinito o semi infinito (o
# a veces porque es la mejor idea que tenemos en ese momento).

# En estas ocasiones, podemos usar la sentencia `break` para detener el bucle.

# Si el `if` tiene 1 solo statement, lo podemos escribir en 1 sola línea.
for i in range(10):
    if i == 6: break
    print(i) # Solo imprimirá hasta el 5

for name in names:
    if name == 'Pablo': break
    print(name)

i = 1
sum = 0
while True:
    if i == 10: break
    sum += i
    i += 1
print(f'{i} -> {sum}') # 10 -> 45. Pero... esto no está bien :(

# Este caso demuestra que el orden de las sentencias importa.
i = 1
sum = 0
while True:
    sum += i
    if i == 10: break
    i += 1
print(f'{i} -> {sum}') # 10 -> 55. Ahora está mejor :D

# Si queremos omitir ejecuciones puntuales de un bucle, se puede usar `continue`.

for number in my_list:
    if number % 3 != 0: continue
    print(number) # Solo imprime los números divisibles entre 3

for name in names:
    if name.find('o') >= 0: continue
    print(name) # Solo imprime los nombres que no contienen 'o'

# Calcular la suma de los números pares menores o igual a 10.
# Este es un ejemplo para mostrar el uso de `continue`. El cálculo se puede
# hacer mejor.
i = 0
sum = 0
while i <= 10:
    i += 1
    if i % 2 == 1: continue
    sum += i
print(sum) # 30

# Python ofrece ejecutar sentencias al finalizar el bucle usando `else`. La
# ventaja de `else` es indicar que no se ha ejecutado `break` dentro del bucle.

for number in my_list:
    if number > 10: break
else:
    print('Todos los números son menores o igual a 10.')

for number in my_list:
    if number <= 10: break
else:
    print('Este mensaje no se imprimirá porque se ejecutó `break`.')
