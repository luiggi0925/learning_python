#########################
#  Clases y OOP básico  #
#########################

# Python ofrece los paradigmas de programación funcionales y orientado a objetos
# (OOP) para escribir aplicaciones. En las lecciones de funciones (
# principalmente en la segunda) hago énfasis en algunas bases de programación
# funcional: high order functions y closures. Ahora entraremos en OOP y las
# ofertas de Python al respecto.

# Desde el vamos comento que OOP de Python no se parece al de C++, Java o C#.
# Está algo alejado de eso, y me parece bien. Encontré algunos mecanismos para
# simular o acercarse a esos lenguajes, pero creo que no se necesitan.

from abc import ABC, abstractmethod
import types
from typing import List


print('Definición de clases y objetos:')

# Las clases se definen así:
#
# class MyClass[(parent_classes...)]:
#     [def __init__(self, <args>) [-> None]:
#         # lógica para la creación de objeto
#     ]
#     def funcion(self):
#         ...
#
# Las bases:
# - `class` es una palabra reservada para definir una clase.
# - `MyClass` es el nombre de mi clase.
# - `[(parent_classes...)]` es opcional para indicar que heredamos de una o
#   múltiples clases.
# - La función `__init__` es el constructor de nuestra clase. Aquí se pueden
#   definir y asignar atributos a la clase. Puede recibir múltiples argumentos.
# - `self` indica la instancia actual de la clase. Es similar a `this` en otros
#   lenguajes (Java, C#, etc).
# - Podemos definir más funciones.

# Definimos nuestra primera clase.
class MyFirstClass:
    def __init__(self) -> None:
        # Podemos definir atributos 'en el aire' en el constructor.
        self.description = 'This is my first class'

# Creamos un objeto de nuestra clase.
first_object = MyFirstClass()
# Accedemos a su atributo. Por defecto los atributos y funciones son públicos.
print(first_object.description)
print(type(first_object)) # <class '__main__.MyFirstClass'>

# La función `isinstance` comprueba si un objeto es de un tipo de clase
# específico.
print(isinstance(first_object, MyFirstClass)) # True

# Por defecto, todas las clases extienden de `object`.
print(isinstance(first_object, object)) # True

print('---------------------------------------------------------------------')
print('Capacidades de las clases:')

# Python provee los siguientes mecanismos en las clases:
#
# Visibilidad (pública, protegida, privada):
# - Por defecto todos los atributos y funciones de una clase son públicos. Los
#   atributos se pueden modificar directamente desde los objetos.
# - Para definir atributos y funciones protegidas, se usa el caracter `_` como
#   prefijo para el nombre. Solo las clases hijas y el módulo actual podrá
#   acceder y modificar estos elementos. Esto es una convención para notificar a
#   otros programadores nuestro propósito para que no usen o modifiquen estos
#   atributos y funciones fuera de la clase.
# - Para definir atributos y funciones privadas, se usa la cadena `__` como
#   prefijo para el nombre. Solo la clase en cuestión podrá acceder, modificar
#   y usar estos elementos. Esto es una convención para notificar a otros
#   programadores de nuestro propósito.
#
# Propiedades en las clases:
# - Se pueden definir propiedades para leer los atributos protegidos y privados.
#   La propiedad será una función que retorne el atributo o una transformación
#   del mismo, decorado con la anotación `@property`. Debe poseer un nombre
#   apropiado para entendimiento de otros programadores. Usualmente el nombre
#   será el del atributo sin el prefijo. También se pueden crear propiedades que
#   resulten de la computación de 1 o más atributos.
# - Las propiedades son de solo lectura. Se puede definir una función `setter`
#   para la operación de escritura. Esta función será decorada con la anotación
#   `@<nombre_propiedad>.setter`
# - Las funciones de las propiedades (getter y setter) deben tener el mismo
#   nombre, y variarán en el tipo de retorno (setter no devuelve resultado) y
#   el argumento (getter no tiene argumentos, setter solo tiene 1).
# - La comunidad Python prefiere evitar nombres como `get<Propiedad>` y
#   `set<Propiedad>` para las funciones de las propiedades.
#
# Funciones útiles:
# Las clases pueden sobreescribir funciones que heredan de `object`. Algunas
# de ellas:
# - `__eq__`. Permite comparar la igualdad entre dos objetos de esta clase.
#   Esta función se usará en `mi_objeto1 == mi_objeto2`.
# - `__str__`. Genera una representación en cadena de texto del objeto para
#   visualización de usuario. Útil para casos como
#   `print(mi_objeto)` o en interpolación de cadenas.
# - `__repr__`. Genera una representación en cadena de texto del objeto para
#   fines de depuración (debugging). Útil para conocer el estado
#   del objeto en revisión.
# - `__ne__`. Compara la desigualdad entre dos objetos de esta clase. Solo se
#   debe reescribir bajo circunstancias específicas. Lo ideal será no
#   sobreescribirlo. Más información: https://stackoverflow.com/q/4352244/1065197
# - `__hash__`. Genera un número entero que identifica esta instancia (hash).
#   Esta función se usa, por ejemplo, dentro de un diccionario para comprobar
#   que la llave es única. Una buena implementación retornará el mismo valor de
#   hash ante múltiples ejecuciones sobre el mismo objeto, aunque su estado
#   cambie. Asimismo, para dos objetos de la misma clase que son considerados
#   iguales `==`, se espera que ambos objetos generen el mismo hash. Sin
#   embargo, dos objetos que generen el mismo hash no tienen que ser iguales.
#   Opinión personal: implementa `__hash__` para clases que poseen estado
#   inmutable. De la misma manera, usa objetos inmutables como llaves para un
#   diccionario.
# - `__bool__`. Indica si el objeto actual se considera Truthy o Falsy.

class Person:
    def __init__(self, name: str, surname: str, age = 0) -> None:
        self._name = name
        self._surname = surname
        self.__age = age
    def respirar(self):
        return f'{self._name} respira'
    def caminar(self):
        return f'{self._name} camina'
    def andar(self):
        return f'{self.respirar()} {self.caminar()}'
    @property
    def age(self):
        return self.__age
    @age.setter
    def age(self, age):
        self.__age = age
    def __str__(self) -> str:
        years = 'año' if self.__age == 1 else 'años'
        return f'{self._name} {self._surname} de {self.__age} {years}.'
    def __repr__(self) -> str:
        return f'Person(_name:{self._name}, _surname:{self._surname}, __age:{self.__age})'
    def __eq__(self, __value: object) -> bool:
        return isinstance(__value, Person) and\
            self._name == __value._name and\
            self._surname == __value._surname and\
            self.__age == __value.__age
    def __bool__(self) -> bool:
        return self._name != None and self._name != ''\
            and self._surname != None and self._surname != ''\
            and self.__age > 0

luiggi = Person('Luiggi', 'Mendoza', 21)
luiggi_clon = Person('Luiggi', 'Mendoza', 21)
print(luiggi) # 'Luiggi Mendoza de 21 años.'
print(luiggi_clon) # 'Luiggi Mendoza de 21 años.'
print(repr(luiggi)) # Person(_name:Luiggi, _surname:Mendoza, __age:21)
print(luiggi_clon == luiggi) # True

# Revisemos algunas propiedades una por una.
# Podemos acceder a los atributos protegidos dentro del módulo.
print(luiggi._name) # 'Luiggi'
luiggi._name += ' Thomas'
# Como tenemos acceso, lo podemos modificar. Esto es ilustrativo, debemos evitar
# este código en aplicaciones reales.
print(luiggi._name) # 'Luiggi Thomas'

# No se puede acceder al atributo privado desde fuera de la clase.
# print(luiggi_clon.__age) # AttributeError: 'Person' object has no attribute '__age'. Did you mean: '_name'?

# En su lugar, se puede usar la propiedad definida.
print(luiggi.age) # 21
# Esta propiedad posee un setter, así que permite modificar su valor.
luiggi.age+=1
print(luiggi.age) # 22
# Al haber mutado el estado del objeto `luiggi`, estos dos objetos no deberían
# ser considerados iguales.
print(luiggi_clon == luiggi) # False
# Nuestro objeto, por ser una instancia, no será `None`.
print(luiggi == None) # False
# Evaluando el Truthyness.
if luiggi: print('Soy True') # 'Soy True'
# Evaluando el Truthyness con una instancia "vacía".
empty_person = Person(None, None)
if empty_person: print('Soy True')
else: print('Soy False') # 'Soy False'
empty_person = Person('', '')
if empty_person: print('Soy True')
else: print('Soy False') # 'Soy False'

print('---------------------------------------------------------------------')
print('Herencia, Clases Abstractas e Interfaces:')

# Vamos a definir una clase padre abstracta y clases hijas.

# Al extender nuestra clase de `abc.ABC`, la clase es automáticamente marcada
# como abstracta. Para crear objetos de dicha clase, se debe implementar todas
# las funciones marcadas como abstractos.
class Employee(ABC):
    def __init__(self, id: int, name: str) -> None:
        self.__id = id
        self.__name = name
    # Indicamos que la función es abstracta.
    @abstractmethod
    def mark_entry(self, entries: list) -> None:
        pass

# Extiende de Employee
class RegularEmployee(Employee):
    def __init__(self, id: int, name: str) -> None:
        super().__init__(id, name)
    # Sobreescribe la función mark_entry definida en la clase padre.
    def mark_entry(self, entries: list) -> None:
        print('Marcando entrada al trabajo como empleado regular')
        entries.append(self)

# Extiende de Employee
class Manager(Employee):
    def __init__(self, id: int, name: str, managed: List[Employee]) -> None:
        super().__init__(id, name)
        # Creamos una copia de la lista
        self.__managed = [*managed]
    # Sobreescribe la función mark_entry definida en la clase padre.
    def mark_entry(self, entries: list) -> None:
        print('Llegando a la hora que quiero... ¡Temprano! Porque hay que dar el ejemplo.')
        entries.append(self)

# No se puede crear una instancia de `Employee`.
# employee = Employee(0, '') # TypeError: Can't instantiate abstract class Employee with abstract method mark_entry

employee1 = RegularEmployee(10, 'Jaime')
employee2 = RegularEmployee(14, 'Adria')
managed_by_phoebe = [employee1, employee2]
employee3 = Manager(19, 'Phoebe', managed_by_phoebe)
entries = []
employee3.mark_entry(entries)
employee2.mark_entry(entries)
employee1.mark_entry(entries)

# Evaluemos el árbol de clases.
print('employee1 es instancia de RegularEmployee:', isinstance(employee1, RegularEmployee)) # True
print('employee2 es instancia de RegularEmployee:', isinstance(employee2, RegularEmployee)) # True
print('employee1 es instancia de Manager:', isinstance(employee1, Manager)) # False
print('employee2 es instancia de Manager:', isinstance(employee2, Manager)) # False
print('employee1 es instancia de Employee:', isinstance(employee1, Employee)) # True
print('employee2 es instancia de Employee:', isinstance(employee2, Employee)) # True
print('employee1 es instancia de object:', isinstance(employee1, object)) # True
print('employee2 es instancia de object:', isinstance(employee2, object)) # True

print('employee3 es instancia de RegularEmployee:', isinstance(employee3, RegularEmployee)) # False
print('employee3 es instancia de Manager:', isinstance(employee3, Manager)) # True
print('employee3 es instancia de Employee:', isinstance(employee3, Employee)) # True
print('employee3 es instancia de object:', isinstance(employee3, object)) # True

# También podemos evaluar si una clase es subclase de otra con la función
# `issubclass()`.
print('RegularEmployee extiende de Employee:', issubclass(RegularEmployee, Employee)) # True
print('Manager extiende de Employee:', issubclass(Manager, Employee)) # True
print('Employee extiende de object:', issubclass(Employee, object)) # True
print('RegularEmployee extiende de object:', issubclass(RegularEmployee, object)) # True
print('Manager extiende de object:', issubclass(Manager, object)) # True
print('RegularEmployee extiende de Manager:', issubclass(RegularEmployee, Manager)) # False
print('Manager extiende de RegularEmployee:', issubclass(Manager, RegularEmployee)) # False

# En Python no existen las interfaces como en otros lenguajes (Java, C#). Lo más
# cercano es una clase abstracta. Esto se debe a que Python funciona bajo la
# filosofía duck typing. Existen mecanismos para tratar una clase abstracta como
# interfaz y restringir que la función `issubclass()` solo devuelva `True`
# cuando la clase implemente todas las funciones abstractas, tal como se discute
# aquí: https://realpython.com/python-interface/

print('---------------------------------------------------------------------')
print('Clases anónimas:')

# Desde Python 3.3 podemos crear clases anónimas mediante la clase
# `types.SimpleNamespace`. Esto realmente devuelve un objeto que pertenece a
# una clase "vacía" que posee ciertas funciones definidas. Muestro cómo podemos
# crear un objeto de clase anónima y forzarlo a que sea una instancia de nuestra
# clase abstracta (gracias al poder del duck typing).

employee = types.SimpleNamespace(id=5, name='Luiggi',\
    mark_entry = lambda entries: entries.append(employee))
print(type(employee))
print(isinstance(employee, Employee)) # False
# Esta función es la que hace la "magia" (yo diría que es magia oscura que
# debe usarse con cuidado).
Employee.register(types.SimpleNamespace)
print(isinstance(employee, Employee)) # True
employee.mark_entry(entries)
print(entries)

print('---------------------------------------------------------------------')
print('Funciones estáticas y de clase:')

# Las funciones estáticas no requieren de una instancia de la clase para ser
# ejecutadas.

class Calc:
    @staticmethod
    def sum(a: int, b: int) -> int:
        return a + b

print(Calc.sum(1, 2)) # 3
print(Calc.sum) # 

# Las funciones de clase reciben como argumento la clase en cuestión. Esto
# permite acceder a datos que pertenecen a la clase. También es un mecanismo
# para crear métodos fábrica (factory methods).

class PythonLesson:
    # Atributo de clase, no de instancia. Su estado afecta a todas las
    # instancias de la clase.
    author = 'Luiggi'
    def __init__(self, title: str, content = 'TBD') -> None:
        # Estos son atributos de instancia.
        self.__title = title
        self.__content = content
    def __str__(self) -> str:
        return f'{self.__title}: "{self.__content}"'
    @classmethod
    def transfer_lessons(cls, new_author: str):
        cls.author = new_author

lesson00 = PythonLesson('Hola mundo')
lesson01 = PythonLesson('Variables')
print(lesson00) # Hola mundo: "TBD"
print(lesson01) # Variables: "TBD"
print(lesson00.author) # 'Luiggi'
print(lesson01.author) # 'Luiggi'
print(PythonLesson.author) # 'Luiggi'
# Los métodos de clase se pueden invocar sin necesidad de usar instancias de la
# clase. En este caso, se cambia el nombre del autor para todas las instancias.
PythonLesson.transfer_lessons('Manolo')
print(lesson00.author) # 'Manolo'
print(lesson01.author) # 'Manolo'
print(PythonLesson.author) # 'Manolo'
