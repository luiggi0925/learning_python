########################################
#  Estructuras de datos: Diccionarios  #
########################################

# El diccionario es una estructura de datos mutable que almacena los elementos
# en pares. Estos pares son conocidos como llave (key) y valor (value).
# A diferencia de las listas y tuplas, el diccionario solo admite conocer los
# elementos mediante las llaves que hemos agregado.
#
# Características de los diccionarios:
# - Agrega los elementos en pares llave-valor.
# - Los elementos se ordenan por orden de ingreso.
# - Acceso por llaves.
# - Se puede sobreescribir el valor de una llave existente.
# - Eliminación de datos con la llave.
# - No permite llaves duplicadas.
# - Permite valores duplicados.
#
# Análisis asintótico de las operaciones con Big O:
#   Operación    Mejor   Promedio   Peor
# -----------------------------------------
#   Inserción:    O(1)     O(1)     O(N)
#   Acceso:       O(1)     O(1)     O(N)
#   Búsqueda:     O(1)     O(1)     O(1)
#   Eliminación:  O(1)     O(1)     O(N)
#

print('Creación de diccionario:')

# El diccionario se crea con llaves `{}`.
my_dictionary = {} # Diccionario vacío.
print(my_dictionary) # {}
print(type(my_dictionary)) # <class 'dict'>
# Las llaves y valores se separan usando `:`.
# Estos pares se separan usando `,`.
my_dictionary = { 'hola':'mundo', 'nombre':'Luiggi' }
print(my_dictionary) # {'hola': 'mundo', 'nombre': 'Luiggi'}

# Otro mecanismo de creación de diccionario. Para diccionarios vacíos, es más
# eficiente usar `{}`.
# Este mecanismo sirve más con iterables. Se verá en una lección futura.
my_dictionary = dict() # Diccionario vacío.
print(my_dictionary) # {}

print('---------------------------------------------------------------------')
print('Agregar elementos:')
# Para agregar o actualizar elementos, se usa la siguiente notación:
# diccionario[llave] = valor
my_dictionary['edad'] = 10
my_dictionary['nombre'] ='Camila'
print(my_dictionary) # {'edad': 10, 'nombre': 'Camila'}

my_dictionary['edad'] += 9 # Se actualiza el valor para esta llave.
print(my_dictionary) # {'edad': 19, 'nombre': 'Camila'}

# La función `len()` nos permite obtener el tamaño del diccionario.
print(len(my_dictionary)) # 2, porque son 2 pares llave-valor.

# Similar a las listas, lo ideal es que todas las llaves sean del mismo tipo.
my_dictionary[5] = 'lo soportará?'
# Por supuesto que lo soporta.
print(my_dictionary) # {'edad': 19, 'nombre': 'Camila', 5: 'lo soportará?'}

print('---------------------------------------------------------------------')
print('Acceso a elementos:')

# Se accede a los elementos mediante la llave:
print(my_dictionary['edad']) # 19
print(my_dictionary[5]) # 'lo soportará?'

# Si accedemos usando una llave que no existe, se lanzará un error.
# print(my_dictionary['no existo']) # KeyError: 'no existo'
# También podemos usar la función `get()`. Si la llave no existe, en lugar de
# lanzar un error devolverá `None`. Además, esta función permite obtener un
# valor por defecto en caso que la llave no existe.
print(my_dictionary.get('no existo')) # None
print(my_dictionary.get('no existo', 'Valor por defecto')) # 'Valor por defecto'
print(my_dictionary.get('edad', 18)) # 19, porque la llave sí existe.

# Un mecanismo más para evaluar si una llave existe es con el operador `in`.
print('no existo' in my_dictionary) # False
print('nombre' in my_dictionary) # True

# Al desempaquetar el diccionario, obtendremos las llaves.
key1, key2, key3 = my_dictionary
print(f'key1={key1} key2={key2} key3={key3}') # keyval1=edad keyval2=nombre keyval3=5

# Para acceder o desempaquetar los valores, usamos la función `values()`.
val1, val2, val3 = my_dictionary.values()
print(f'val1={val1} val2={val2} val3={val3}') # val1=19 val2=Camila val3=lo soportará?

# Podemos obtener todos los datos del diccionario como un conjunto de
# tuplas llave-valor usando la función `items()`.
print(my_dictionary.items()) # dict_items([('edad', 19), ('nombre', 'Camila'), (5, 'lo soportará?')])
keyval1, keyval2, keyval3 = my_dictionary.items()
print(keyval1, keyval2, keyval3) # ('edad', 19) ('nombre', 'Camila') (5, 'lo soportará?')

print('---------------------------------------------------------------------')
print('Concatenación de diccionarios:')

# Podemos unir el contenido de 2 diccionarios con el operador `|`.
first_dict = { 'nombre':'Luiggi' }
second_dict = { 'edad':'58' }
result = first_dict | second_dict # {'nombre': 'Luiggi', 'edad': '58'}
print(result)

# También podemos adicionar los contenidos en un diccionario existente con la
# función `update()`.
product_data = { 'nombre': 'Silla acrílica' }
product_new_data = { 'precio': 15 }
print(product_data) # {'nombre': 'Silla acrílica'}
product_data.update(product_new_data)
print(product_data) # {'nombre': 'Silla acrílica', 'precio': 15}

# `update()` también permite sobreescribir (actualizar) datos existentes.
product_new_data = { 'precio': 13.5, 'descuento': 0.1 }
product_data.update(product_new_data)
print(product_data) # {'nombre': 'Silla acrílica', 'precio': 13.5, 'descuento': 0.1}

print('---------------------------------------------------------------------')
print('Eliminar elementos:')
# Similar a una lista, podemos eliminar elementos usando la función `pop(llave)`
# o el operador `del`.
product_data.pop('precio')
print(product_data) # {'nombre': 'Silla acrílica'}
del product_data['descuento']
print(product_data) # {'nombre': 'Silla acrílica', 'precio': 13.5}

# En ambos casos, si la llave a eliminar no existe, se lanzará un error.
# product_data.pop('descuento') # KeyError: 'descuento'
# del product_data['descuento'] # KeyError: 'descuento'

# Podemos remover el último elemento del diccionario usando la función
# `popitem()`.
product_data.update(product_new_data)
item = product_data.popitem()
print(item) # ('descuento', 0.1)

# Se pueden eliminar todos los elementos del diccionario con la función `clear()`.
product_data.update(product_new_data)
product_data.clear()
print(product_data) # {}

print('---------------------------------------------------------------------')
print('Conversión a lista:')

# Todas las conversiones se realizan con la función `list()`.

# Convertir todas las llaves del diccionario.
keys = list(my_dictionary)
print(keys) # ['edad', 'nombre', 5]

# Convertir todos los valores del diccionario.
values = list(my_dictionary.values())
print(values) # [19, 'Camila', 'lo soportará?']

# Convertir todos los pares llave-valor.
keyvals = list(my_dictionary.items())
print(keyvals) # [('edad', 19), ('nombre', 'Camila'), (5, 'lo soportará?')]
