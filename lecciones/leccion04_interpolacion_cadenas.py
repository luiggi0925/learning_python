##############################
#  Interpolación de cadenas  #
##############################

# Este es un proceso para generar cadenas usando variables conocidas

# Tenemos estas 2 variables conocidas.
name, surname = 'Luiggi', 'Mendoza'

# Queremos generar cadenas en base a una plantilla. Ejemplo "Bienvenido Luiggi Mendoza!"
# También queremos que, si cambian los valores de las variables, se mantenga
# el contenido de la plantilla. Ejemplo: "Bienvenido Guapo Desconocido!"

print('Interpolación con f\'<plantilla>\':')
# Me gusta esta interpolación porque usa las variables directo en la plantilla.
print(f'Bienvenido {name} {surname}!') # Bienvenido Luiggi Mendoza!

# Si cambio los valores, seguirá funcionando
name, surname = 'Guapo', 'Desconocido'
print(f'Bienvenido {name} {surname}!') # Bienvenido Guapo Desconocido!

# Devolveremos las variables a su valor inicial
name, surname = 'Luiggi', 'Mendoza'


print('---------------------------------------------------------------------')
print('Interpolación con `str.format()`:')
# Otro método de interpolación es mediante la función `str.format()`. Posee múltiples formas.

# Se usa `{}` como marcador para una variable.
# No me gusta porque es muy fácil de equivocarse al colocar el orden de las variables
print('Bienvenido otra vez {} {}'.format(name, surname)) # Bienvenido otra vez Luiggi Mendoza
# USando las variables al revés, cambiando el sentido de la plantilla.
print('Bienvenido otra vez {} {}'.format(surname, name)) # Bienvenido otra vez Mendoza Luiggi
# Un ejemplo más real de cómo esto genera problemas
product_name, price, discount = 'Televisor 55 pulgadas', 2000, 10
print('Aproveche la oferta de {}. ¡Descuento de {}%! Con un precio de ${}'.format(product_name, price, discount))

# Para resolver este problema, se pueden usar parámetros con nombre
print('Bienvenida vuestra merced {surname} {name}'.format(name=name, surname=surname))
print('Aproveche la oferta de {product_name}. ¡Descuento de {discount}%! Con un precio de ${price}'\
    .format(product_name=product_name, price=price, discount=discount)) # No importa el orden de las variables

# Otro problema de `str.format()` es que si se omite algún parámetro, da error.
# KeyError: 'surname'
# print('Bienvenida vuestra merced {surname} {name}'.format(name=name))

print('---------------------------------------------------------------------')
print('Interpolación con `string.Template`:')
# Los imports pueden colocarse en cualquier parte del programa.
# Lo ideal es que vayan al inicio.
# Se pone aquí solo como ejemplo, pero en futuras lecciones todos los import irán al inicio.
# En la lección 13 ahondaré más en los imports.
from string import Template

# Una manera más de generar cadenas a partir de plantillas es mediante `string.Template`
template = Template('Aloha $name $surname. Este es un ejercicio de Python')
print(template.substitute(name=name, surname = surname))
# Al igual que con `str.format()`, se lanza un error si se omiten parámetros
# print(template.substitute(name=name))

print('---------------------------------------------------------------------')
print('Interpolación multi línea:')
# También se pueden usar cadenas multi línea para la interpolación.
multiline_sample = f'''Bienvenido
{name} {surname}
Será un placer atenderlo'''
print(multiline_sample)

# Documento esto porque me sucedió en un ejercicio.
# Se puede usar una cadena para devolver una cadena JSON.
# Estas cadenas usualmente contienen llaves (`{` y `}`).
# Estos caracteres deben ser escapados usando doble {{ y }}
# Ejemplo:
my_json_string = f'''{{
    "name": {name},
    "surname": {surname}
}}'''
print(my_json_string)
