##########################
#  Clásico 'hola mundo'  #
##########################
print('Hola mundo')

# La función `print()` nos permite imprimir texto en el terminal. Es la manera
# visual de revisar nuestros avances durante el proceso de aprendizaje.

# El caracter `#` indica el inicio de un comentario.
# El intérprete ignora todo lo que está en comentarios.
# Los comentarios facilitan la comunicación entre desarrolladores.
# La limitante del `#` es que aplica solo en una línea.
# Para múltiples líneas, se usa triple `'` o triple `"` al inicio y fin del bloque.
"""
Ejemplo de comentario
en múltiples líneas
"""
'''
Otro ejemplo de comentario
en múltiples líneas
'''
