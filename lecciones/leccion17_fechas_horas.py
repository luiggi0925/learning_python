####################
#  Fechas y horas  #
####################

import datetime
import pytz
from babel.dates import format_datetime

# En las aplicaciones del mundo real se trabaja con múltiples datos y tipos. Un
# dato común en las aplicaciones es la fecha y hora. Este tipo sirve para
# múltiples casos:
# - Momento en que se crea un objeto
# - Duración de una tarea
# - Medición de tiempo (rendimiento)
# - Revisar si ha expirado un certificado digital
# - Etc.
#
# Vamos a entrar en algunas funciones para crear y manipular fechas.

print('Fecha y hora:')

# Obtener la fecha y hora actual del sistema.
now = datetime.datetime.now()
print(now) # Ejemplo 2023-04-25 21:59:32.508185
print(type(now)) # <class 'datetime.datetime'>

# Obtener una fecha y hora específica. Por defecto siempre se obtendrán en la
# zona horaria del sistema.
valentine = datetime.datetime(year=2023, month=2, day=14, hour=9, minute=34, second=23)
print(f"2023 year's valentine is {valentine}")

# La clase `timedelta()` está asociada a cambios en las fechas. Se usa para
# medir la diferencia entre 2 fechas, por año, mes, día, hora, minuto, segundo
# y microsegundo.
tomorrow = now + datetime.timedelta(days=1)
print(tomorrow) # Ejemplo 2023-04-26 22:20:24.358628
yesterday = now - datetime.timedelta(days=1)
print(yesterday)
print(tomorrow - yesterday) # 2 days, 0:00:00
print(type(tomorrow - yesterday)) # <class 'datetime.timedelta'>

# La zona horaria hace que dos horas "diferentes" sean la misma. Ejemplo:
# 2023-04-25 22:24:00.0 GMT+2 (Madrid) es igual a
# 2023-04-25 15:24:00 GMT-5 (Lima).
# Para esto, usaremos el módulo `pytz` que nos ayudará a configurar las zonas
# horarias.
datetime_madrid = datetime.datetime(2023, 4, 20, 22, 24)
datetime_madrid = tzinfo=pytz.timezone('Europe/Madrid').localize(datetime_madrid)
datetime_lima = datetime.datetime(2023, 4, 20, 15, 24)
datetime_lima = tzinfo=pytz.timezone('America/Lima').localize(datetime_lima)
print(datetime_madrid == datetime_lima)
# Aquí imprimiremos las fechas en el formato timezone del sistema.
print(datetime_madrid.astimezone())
print(datetime_lima.astimezone())

# Podemos ver todas las zonas horarias disponibles en el módulo al revisar esta
# lista
# print(pytz.all_timezones)

# Las fechas y horas tienen operaciones similares usando datetime.date (fecha) 
# y datetime.time (hora).
today = datetime.date.today()
print(today) # Ejemplo 2023-04-25
print(type(today)) # <class 'datetime.date'>

now_time = datetime.time()
print(now_time) # 00:00:00
print(type(now_time)) # <class 'datetime.time'>
now_time = datetime.datetime.now().time()
print(now_time) # 23:02:58.129043

print('---------------------------------------------------------------------')
print('Conversión entre texto y fecha/hora:')

# Algo que suele pasar es confundir la representación en cadena de una fecha y
# hora (para esta sección, simplemente fecha) con el valor concreto de la fecha.
# Una fecha puede tener múltiples representaciones en forma de texto. Sin
# considerar que dos fechas pueden ser iguales porque difieren en la hora (a
# veces en el día) pero están en la misma zona horaria (como vimos hace poco).
#
# El formato soportado para fechas y horas en Python se encuentra en
# https://docs.python.org/3/library/datetime.html#strftime-and-strptime-behavior

# Esta es la misma fecha en 4 representaciones distintas de texto, asumiendo que
# todas están en la misma zona horaria.
date_format_1 = '2023-04-01 15:05:12'
date_format_2 = '01/04/23 03:05:12pm'
date_format_3 = '15:05:12 04/01/2023'

# Para convertir texto a fecha se usa la función `datetime.strptime()`.
dt1 = datetime.datetime.strptime(date_format_1, '%Y-%m-%d %H:%M:%S')
print(dt1) # 2023-04-01 15:05:12

dt2 = datetime.datetime.strptime(date_format_2, '%d/%m/%y %I:%M:%S%p')
print(dt2) # 2023-04-01 15:05:12

dt3 = datetime.datetime.strptime(date_format_3, '%H:%M:%S %m/%d/%Y')
print(dt3) # 2023-04-01 15:05:12

# Al margen de los resultados de la impresión, comprobemos que efectivamente
# son iguales.
print(dt1 == dt2 == dt3) # True

# La función `datetime.strftime()` nos permite convertir una fecha en una
# representación de texto. Ejemplo:
print(dt1.strftime('%a %d %b %y %I:%M:%S %p')) # Sat 01 Apr 23 03:05:12 PM

# Puede que te pase como a mí, que el nombre del día de la semana y del mes
# estén impresos en inglés. ¿Por qué? Porque al parecer es el 'locale' que usa
# Python para realizar esta conversión.
# 
# El 'locale' afecta directamente sobre el formato usado para representar
# fechas, números, monedas, etc, de nuestra aplicación. Para el caso de las
# fechas, aplica sobre la generación y reconocimiento de nombres de días y
# meses.
# 
# Si tu aplicación solo debe soportar UN ÚNICO locale, lo ideal es configurar
# ese locale como parte de las primeras sentencias de la aplicación usando
# `locale.setlocale()`.
#
# En aplicaciones más complejas es posible que debas soportar múltiples locale.
# Para facilitar esta labor, puedes usar una librería externa como babel. Cabe
# considerar que esta librería usa su propia sintaxis para formato de fechas:
# https://babel.pocoo.org/en/latest/dates.html

# Representación de la misma fecha anterior pero en ruso usando babel.
print(format_datetime(dt1, format='EEE dd MMM yy hh:mm:ss a', locale='ru_RU')) # сб 01 апр. 23 03:05:12 PM
# Ahora en japonés.
print(format_datetime(dt1, format='EEE dd MMM yy hh:mm:ss a', locale='ja_JP')) # 土 01 4月 23 03:05:12 午後
# Ahora en castellano.
print(format_datetime(dt1, format='EEE dd MMM yy hh:mm:ss a', locale='es_ES')) # sáb 01 abr 23 03:05:12 p. m.
# Un mismo idioma puede tener diferentes culturas. Por ejemplo, castellano
# peruano.
print(format_datetime(dt1, format='EEE dd MMM yy hh:mm:ss a', locale='es_PE')) # sáb 01 abr. 23 03:05:12 p. m.
# Castellano colombiano.
print(format_datetime(dt1, format='EEE dd MMM yy hh:mm:ss a', locale='es_CO')) # sáb 01 abr 23 03:05:12 p. m.
# Castellano argentino.
print(format_datetime(dt1, format='EEE dd MMM yy hh:mm:ss a', locale='es_AR')) # sáb 01 abr 23 03:05:12 p. m.
# Castellano uruguayo.
print(format_datetime(dt1, format='EEE dd MMM yy hh:mm:ss a', locale='es_UY')) # sáb 01 abr. 23 03:05:12 p. m.
# Castellano mexicano.
print(format_datetime(dt1, format='EEE dd MMM yy hh:mm:ss a', locale='es_MX')) # sáb 01 abr 23 03:05:12 p. m.
