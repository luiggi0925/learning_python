##################################
#  Estructuras de datos: Listas  #
##################################

# En aplicaciones más cercanas al mundo real, se necesita trabajar con
# múltiples datos asociados entre ellos. Por ejemplo:
# - Los alumnos de un aula de clase.
# - Los productos para mostrar en una vitrina.
# - Las características de calles de una ciudad.
# - Etc.
# Un detalle en común de estos ejemplos es que no podemos predecir la cantidad
# de datos que usaremos.
#
# En estas situaciones, lo ideal es usar una estructura de datos.
# Una estructura de datos permite almacenar datos para su posterior búsqueda.
# Existen múltiples estructuras de datos, que se adaptan a las características
# de los datos a almacenar o a los mecanismos de búsqueda.
#
# Una de las estructuras de datos más común y simple es la lista. Permite
# almacenar los datos de manera continua y acceder a ellos por un índice numérico.
# Una lista sería similar a un cajón con secciones contiguas, donde los datos se
# agregarán en cada sección hasta llenar el cajón.
# A diferencia del cajón, la lista puede crecer de forma dinámica para almacenar
# más elementos.
#
# El acceso a los elementos de una lista es por índice. El primer índice de una
# lista es 0. El último índice es `len(lista) - 1`.
# Las búsquedas en una lista se realizan accediendo a cada elemento de la lista
# hasta encontrar el elemento deseado.
#
# IMPORTANTE: Las listas son estructuras de datos mutables. Es decir, permite
# que se agreguen nuevos elementos, se reemplacen o se eliminen los existentes.
#
# Características de las listas:
# - Inserción al final de la lista (por defecto).
# - Acceso por índice. Los índices son numéricos y van de 0 a `len(lista) - 1`
# - Actualización al sobreescribir los datos del índice.
# - Eliminación de datos con el índice.
# - Permite elementos duplicados.
# - No ordena ni evalúa los elementos agregados, actualizados o eliminados.
#
# Análisis asintótico de las operaciones con Big O:
#   Operación    Mejor   Promedio   Peor
# -----------------------------------------
#   Inserción:    O(1)     O(N)     O(N)
#   Acceso:       O(1)     O(1)     O(1)
#   Búsqueda:     O(1)     O(N)     O(N)
#   Eliminación:  O(1)     O(N)     O(N)
#

print('Creación de lista:')
# Podemos crear una lista usando corchetes `[]`.
my_list = [] # Lista vacía.
print(my_list) # []
print(type(my_list)) # <class 'list'>
my_list = [1, 2, 3] # Lista con 3 elementos.
print(my_list) # [1, 2, 3]

# Otro mecanismo de creación de lista. Si queremos crear una lista vacía,
# es más eficiente usar `[]`.
# Este mecanismo sirve más con iterables. Se verá en una lección futura.
my_list = list() # Lista vacía.
print(my_list) # []

print('---------------------------------------------------------------------')
print('Agregar elementos:')
# La lista permite agregar elementos de distintos tipos. Para agregar elementos
# usamos la función `append()`.
my_list.append(5) # Se agrega el número 5.
my_list.append('hola') # Se agrega la cadena 'hola'.
print(my_list) # [5, 'hola']

# La función `len()` nos permite obtener el tamaño de la lista.
print(len(my_list)) # 2
my_list.append(False) # Se agrega el bool False.

# Una lista puede almacenar todo tipo de datos. Incluso otra lista.
my_list.append([]) # Se agrega una lista vacía.
my_list.append(20) # Se agrega una lista vacía.
print(len(my_list)) # 5
print(my_list) # [5, 'hola', False, [], 20]

print('---------------------------------------------------------------------')
print('Acceso a elementos:')

# Podemos acceder a los elementos mediante el índice. El primer índice es 0.
print(my_list[0]) # 5, el primer elemento de la lista.
print(my_list[1]) # 'hola'

# Al igual que en las cadenas, podemos acceder usando un índice negativo.
print(my_list[-1]) # 20

# Si intentamos acceder a un índice mayor o igual al tamaño de la lista,
# se lanzará un error.
# print(my_list[5]) # IndexError: list index out of range
# Sucederá lo mismo con índices negativos.
# print(my_list[-6]) # IndexError: list index out of range

# Se pueden actualizar los elementos de la lista usando el índice
my_list[0] = -4
print(my_list[0])

# También se pueden declarar variables que serán asignadas a partir de
# los elementos de la lista. Esto se llama desempaquetado.
var1, var2, var3, var4, var5 = my_list
print(f'var1={var1} var2={var2} var3={var3} var4={var4} var5={var5}')

# Si desempaquetamos la lista en menos variables de los elementos que posee,
# se lanzará un error.
# var1, var2 = my_list # ValueError: too many values to unpack (expected 2)

print('---------------------------------------------------------------------')
print('Búsqueda de elementos:')

# Las listas proveen funciones de búsqueda sobre los elementos que contiene.
# Si bien las listas soportan cualquier tipo de dato, lo ideal es que contengan
# elementos del mismo tipo.
# Tendremos una lista con valores numéricos y otra con cadenas.
int_list = [10, 5, 21, 7, 2, 8, 14, 5, 6, 10, 2, 9, 21]
str_list = ['manzana', 'pera', 'coco', 'tomate', 'uva', 'naranja']

# `index()` devuelve el primer índice donde se encuentre el elemento a buscar.
print(int_list.index(2)) # 4
print(str_list.index('naranja')) # 5
# Si el elemento a buscar no existe, se lanzará un error.
# print(str_list.index('ciruela')) # ValueError: 'ciruela' is not in list

# `index()` también permite un segundo argumento, el índice para empezar la búsqueda.
# Este índice es inclusivo.
print(int_list.index(2, 4)) # 4
print(int_list.index(2, 5)) # 10

# `count()` devuelve la cantidad de veces que se encuentra el elemento en la lista.
print(int_list.count(5)) # 2
print(str_list.count('coco')) # 1
print(str_list.count('ciruela')) # 0

print('---------------------------------------------------------------------')
print('Concatenación de listas:')
# Podemos unir dos listas en una sola. Esto sirve, por ejemplo, cuando tenemos
# datos similares (con el mismo significado) que provienen de distintas fuentes.
# Para unirlas, existe el operador `+`.

first_list = [10, 12, 8]
second_list = [16, 18, 14]
# Se crea una nueva lista con el contenido de ambas listas.
result_list = first_list + second_list
print(result_list) # [10, 12, 8, 16, 18, 14]

# También se pueden adicionar los contenidos a una lista existente
# con la función `extend()`.
students = ['Luisa', 'Juan', 'Pedro']
new_students = ['Mayra', 'Felipe']
students.extend(new_students)
print(students) # ['Luisa', 'Juan', 'Pedro', 'Mayra', 'Felipe']

print('---------------------------------------------------------------------')
print('Eliminar elementos:')

# Existen ocasiones en que los elementos no se requieren más en la lista.
# Los podemos eliminar de dos maneras.

# La función `pop()` remueve un elemento de la lista por índice.
# También devuelve dicho elemento.
numbers = [10, 20, 30]
num = numbers.pop(1)
print(numbers) # [10, 30]
print(num) # 20
# Si no se indica el índice, por defecto elimina y devuelve el último elemento.
num = numbers.pop()
print(numbers) # [10]
print(num) # 30
# También podemos omitir almacenar el resultado de `pop()`.
numbers.pop()
print(numbers) # []
# Si invocamos `pop()` en una lista vacía, se lanzará un error.
# numbers.pop() # IndexError: pop from empty list

# Otro mecanismo para eliminar un elemento es `del`.
# Este solo remueve, no permite obtener el elemento removido.
numbers = [10, 20, 30]
del numbers[1]
print(numbers) # [10, 30]

# ¡Cuidado! `del` remueve las variables por completo.
# Si usamos `del variable`, no podremos acceder a la variable
del numbers
# print(numbers) # NameError: name 'numbers' is not defined

print('---------------------------------------------------------------------')
print('Ordenar listas:')

# La lista posee la función `sort()` que permite ordenar los datos de
# menor a mayor. Para que esto funcione, los elementos de la lista deben ser
# comparables entre sí. Este es otro motivo para que los datos de la lista sean
# del mismo tipo.

print(int_list) # [10, 5, 21, 7, 2, 8, 14, 5, 6, 10, 2, 9, 21]
int_list.sort()
print(int_list) # [2, 2, 5, 5, 6, 7, 8, 9, 10, 10, 14, 21, 21]

print(str_list) # ['manzana', 'pera', 'coco', 'tomate', 'uva', 'naranja']
str_list.sort()
print(str_list) # ['coco', 'manzana', 'naranja', 'pera', 'tomate', 'uva']

# Una lista con tipos de datos que no se pueden comparar, no permite ser
# ordenada de forma natural.
scrambled_types_list = ['hola', 10, 'mundo', 20]
# scrambled_types_list.sort() # TypeError: '<' not supported between instances of 'int' and 'str'

# En casos que necesitemos un orden particular, `sort()` permite recibir
# una función para comparar los elementos. Esto lo veremos en detalle en
# una lección posterior.

# Dato adicional: el algoritmo para ordenar los datos se llama Timsort. Puedes
# leer más detalles de su funcionamiento en https://en.wikipedia.org/wiki/Timsort
