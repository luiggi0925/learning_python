#####################################
#  Variables y tipos de datos base  #
#####################################

# Las variables sirven para almacenar datos de nuestro programa.
# Primero se declaran e inicializan. Luego se podrán usar.
# Su tipo de dato será el del valor asignado.

print('Tipos de datos soportados en Python:')
# Tipos base
number = 10 # número entero
floating = 1.5 # número con punto flotante, o decimal
string = 'Aloha' # cadena de texto (simplemente cadenas)
another_string = "Aloha!" # la cadena de texto también puede declararse con doble comilla ""
this_is_true = True # booleano, verdadero o falso
complex_number = 1 + 2j # números complejos

# Tipos de colecciones
list = [] # lista, una colección de datos mutable
tuple = () # tupla, una colección de datos immutable (no cambia)
dictionary = {} # diccionario (o mapa), una colección de datos en llave-valor
set = set() # set, un conjunto de datos únicos, no contiene elementos repetidos

# Funciones
def function(): # funcion, permite reusar un bloque de código
    pass

# Tipo especial
empty = None # vacío, en Python no existe el nulo (null, undefined, etc, estos no existen)

# Podemos revisar el tipo de cada variable usando la función `type()`
print( type(number) ) # <class 'int'>
print( type(floating) ) # <class 'float'>
print( type(string) ) # <class 'str'>
print( type(another_string) ) # <class 'str'>
print( type(this_is_true) ) # <class 'bool'>
print( type(complex_number) ) # <class 'complex'>
print( type(list) ) # <class 'list'>
print( type(tuple) ) # <class 'tuple'>
print( type(dictionary) ) # <class 'dict'>
print( type(set) ) # <class 'set'>
print( type(function) ) # <class 'function'>
print( type(empty) ) # <class 'NoneType'>

print('---------------------------------------------------------------------')
print('Tipado dinámico:')
# Python posee tipos de datos dinámicos. Es decir, se puede cambiar el tipo de
# dato de una variable con cambiar su valor.
im_a_number = 10
print( type(im_a_number) ) # <class 'int'>
# Cambiando su valor a str
im_a_number = '10'
print( type(im_a_number) ) # <class 'str'>

# En Python 3 se puede declarar el tipo de una variable, pero es para facilitar
# la comunicación con otros desarrolladores. El lenguaje no fuerza un tipado
# estático.
must_be_a_number: int = '50' # 'Ja ja ja' se ríe el lenguaje.
print( type(must_be_a_number) ) # <class 'str'>

# No se puede usar una variable sin haber sido inicializada
# La línea de abajo lanza error
# print(x) # NameError: name 'x' is not defined

print('---------------------------------------------------------------------')
print('Operaciones con números:')
# Los números permiten operaciones matemáticas
a = 10
b = 15
# Suma
print(a + b) # 25
# Resta
print(a - b) # -5
# Multiplicación
print(a * b) # 150
# División
print(b / a) # 1.5
# Módulo, o dividendo
print(b % a) # 5
# Potencia
print(2 ** 2) # 4
print(3 ** 2) # 9
print(3 ** 4) # 81
print(a ** b) # 1000000000000000

print('---------------------------------------------------------------------')
print('Cadenas multi línea:')
# Las cadenas extensas se pueden definir en múltiples líneas usando triple `'` o triple `"`
# Lo de abajo es una cadena de múltiples líneas. No es un comentario de bloque.
multiline_string = '''SELECT *
FROM table
WHERE col1 = ?
'''
print(multiline_string) # Se respetan los espacios y saltos de línea.
# La diferencia entre comentario de bloque y cadena multi línea es la asignación a una variable.
