###########################
#  Funciones con cadenas  #
###########################

# Las cadenas son inmutables. Esto significa que no pueden cambiar su valor.
# Al aplicar las funciones en ellas, se obtendrá una nueva cadena con
# el nuevo valor.

# Las cadenas se pueden concatenar con el operador `+`
string1 = 'Hola '
string2 = 'mundo!'
string3 = string1 + string2
print(string3) # Hola mundo!
print(string1 + string2) # Hola mundo!

# Para los ejemplos, obtenemos una cadena extensa de https://www.lipsum.com/
long_string = 'lorem ipsum dolor sit amet, consectetur adipiscing elit. nunc eros leo, condimentum eu congue id, aliquam vitae massa.'

print(long_string)

# ¡Convertir todo a MAYÚSCULAS! (Disculpa, no estoy gritando).
print(long_string.upper()) # LOREM IPSUM...
# Convertir solo la primera letra en mayúsculas
print(long_string.capitalize()) # Lorem ipsum...

# `count()` permite conocer cuántas veces existe una subcadena dentro de
# nuestra cadena
# Buscar cuántas 'o's existen.
print(long_string.count('o')) # 8
# También se pueden buscar subcadenas de 2 o más caracteres. Ejemplo con 'um's.
print(long_string.count('um')) # 2

# `find()` permite obtener el primer índice de una subcadena.
print(long_string.find('em')) # 3
print(long_string.find('um')) # 9
# En la línea 24 vimos que 'um' existe 2 veces. ¿Cómo obtener el segundo índice?
# `find()` permite indicar que empiece la búsqueda desde un índice específico.
print(long_string.find('um', 10)) # 81

# `rfind()` es similar a find, pero hace la búsqueda en reversa.
print(long_string.rfind('um')) # 81

# `istitle()` confirma si una cadena es apta para un título.
print(long_string.istitle()) # False
print('Mario Bros, La Película'.istitle()) # True

# `len()` permite obtener la longitud de una cadena. Es una operación constante O(1).
print(len(long_string))

# `__len__()` Es una función privada de las cadenas y no debe usarse directamente.
# La siguiente línea es únicamente para pruebas y comprobar que `len()` no recorre toda la cadena para saber su longitud.
# Evita el código de este tipo en código del mundo real.
# print(long_string.__len__())

# `swapcase()` cambia las letras mayúsculas por minúsculas.
# Como se ve, las funciones de las cadenas se pueden concatenar.
print(long_string.capitalize().swapcase()) # lOREM IPSUM...

# `startswith()` evalúa si los primeros caracteres de la cadena coinciden
# con los caracteres de otra.
# Es sensible a mayúsculas-minúsculas (case sensitive).
print(long_string.startswith('Lo')) # False
print(long_string.startswith('lo')) # True

# ¿Qué pasaría si desconocemos el contenido de letras mayúsculas o minúsculas?
# ¿Cómo podríamos evaluar si realmente contiene la cadena que buscamos?
# `casefold()` retorna una cadena para usar sin preocuparse de mayúsculas o minúsculas.
print(long_string.casefold()) # lorem ipsum...
# Esto permite usar `casefold()` y luego `startswith()` o `find()` sin preocuparnos.
print(long_string.casefold().startswith('lo')) # True
print(long_string.casefold().find('um')) # 9
# A modo de prueba, convertiremos la cadena a mayúsculas y luego aplicaremos `casefold()`
print(long_string.upper().casefold().startswith('lo')) # True
print(long_string.upper().casefold().find('um')) # 9

# `split()` divide la cadena en base a una subcadena.
# Por ejemplo, se puede dividir por '.' para conocer las oraciones que contiene.
print(long_string.split('.'))

# `join()` une múltiples cadenas con un separador.
print("?".join(long_string.split('.'))) # Convierte las oraciones en preguntas
