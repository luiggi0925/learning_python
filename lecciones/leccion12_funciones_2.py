#########################
#  Funciones - Parte 2  #
#########################

# Python considera las funciones como ciudadanos de primera clase. Esto
# significa que las funciones también son consideradas objetos, al igual que
# nuestras variables. Lo comprobados usando `type(alguna_funcion)` en la lección
# anterior. Esta lección usaremos este concepto para ver otras características
# para vincular funciones.

print('---------------------------------------------------------------------')
print('Funciones de alto orden:')

# En la lección anterior, los argumentos recibidos en las funciones siempre
# fueron objetos de tipo 'int', 'bool', 'str', 'list' etc.
# ¿Será que podemos enviar funciones como argumentos?

# La respuesta es: sí. Este mecanismo se usa cuando nuestra función conoce la
# base del comportamiento a ejecutar. Sin embargo, existe parte desconocida o
# que queremos proponer a los usuarios de nuestras funciones para que lo definan.

# Empecemos con un ejemplo demostrativo. La función `show()` recibe dos
# argumentos:
# - name: str. Un nombre para imprimir.
# - how_to_show_function: Una función que debe recibir una cadena. No sabemos
#   qué hará la función específicamente con esa cadena, pero será parte del
#   comportamiento de nuestra función general.
# La función `show()` por recibir una función como argumento se conoce como
# función de alto orden, o en inglés high order function.
def show(name: str, how_to_show_function)-> None:
    print(f"I'm going to show you some magic, {name}.")
    print("I'll invoke this argument function.")
    how_to_show_function(name.upper())

# A modo de prueba, podemos usar `print` para completar nuestra función.
show('Hugo', print) # Nótese que se usa `print`, sin paréntesis.

# Un ejemplo de funciones que usan otra función como parte de su comportamiento
# es la función `sort()` de las listas. Podemos cambiar la manera en cómo se
# evaluarán los elementos a ser ordenados.

my_list = ['cama', 'coche', 'edificio', 'lenteja', 'morral']
my_list.sort()
print(my_list) # Lista ordenada de menor a mayor.
# Definimos nuestra función para redefinir la evaluación de los elementos.
def compare_str_len(element: str) -> int:
    return len(element)
# Usamos nuestra función. Nuevamente, sin paréntesis.
my_list.sort(key=compare_str_len)
print(my_list) # Lista ordenada por el tamaño de las cadenas.

# Veamos un ejemplo más cercano a código real.
# En la compañía se usa mailchimp para enviar correos.
def send_mail_mailchimp(mail_data) -> None:
    print('Enviar correo usando mailchimp.')
# Sin embargo, nos piden integrarnos con sendgrid por un posible cambio de
# plataforma.
def send_mail_sendgrid(mail_data) -> None:
    print('Enviar correo usando sendgrid.')
# Para enviar correos, preparamos los datos y un argumento adicional que indica
# la estrategia para enviar correos, usando mailchimp por defecto.
def welcome_user_notification(user_data, \
    send_mail_function = send_mail_mailchimp):
    print(f'Preparamos los datos del correo con {user_data}')
    # `pass` permite cerrar la definición de un bloque de código. De modo que
    # la función `transform()` sea una función vacía.
    def transform(user_data):
        pass
    mail_data = transform(user_data)
    send_mail_function(mail_data)

welcome_user_notification(('Luiggi', 'luiggi@email.com')) # Se envía por mailchimp
welcome_user_notification(('Luiggi', 'luiggi@email.com'), send_mail_sendgrid) # Se envía por sendgrid


print('---------------------------------------------------------------------')
print('Expresiones lambda:')

# Existen ocasiones que nuestras funciones son para un uso específico y solo
# requerimos los argumentos y una sentencia de código. Por ejemplo, la función
# para cambiar la base del ordenamiento de la lista.

# Estas funciones de 1 sola sentencia se pueden escribir como lambdas, también
# conocidas como funciones anónimas. Definición:
# lambda <argumentos>: <sentencia>

my_list = ['arete', 'bolichera', 'cama', 'edificio', 'lenteja', 'morral']
# La función `compare_str_len()` la escrita como lambda
compare_str_len_lambda = lambda elem: len(elem)
my_list.sort(key=compare_str_len_lambda)
print(my_list) # ['cama', 'arete', 'morral', 'lenteja', 'edificio', 'bolichera']
# Las funciones lambdas también se pueden escribir directamente donde se
# necesitan

my_list = ['arete', 'bolichera', 'cama', 'edificio', 'lenteja', 'morral']
my_list.sort(key=lambda elem: len(elem))
print(my_list) # ['cama', 'arete', 'morral', 'lenteja', 'edificio', 'bolichera']

print('---------------------------------------------------------------------')
print('Funciones anidadas y closures:')

# Así como vimos que los argumentos de las funciones son objetos, las variables
# y el dato de retorno de una función también son objetos. Esto significa que,
# si una función es un objeto, y podemos definir objetos dentro de una función,
# entonces podemos definir una función dentro de otra función.

# Calma, esto es más sencillo de lo que parece.

# Ejemplo:
# Definimos la función `calc()` que puede sumar y multiplicar
def calc(a: int, b: int, operation: str) -> int:
    # Para facilitar el código de `calc()`, definimos las funciones `sum()` y
    # `multiply` dentro de ella.
    def sum(x, y): # Función anidada.
        return x + y
    def multiply(x, y): # Función anidada.
        return x * y
    # Luego, si encontramos la operación solicitada, ejecutamos alguna de las
    # funciones que hemos definido internamente.
    match operation:
        case '+': return sum(a, b)
        case '*': return multiply(a, b)
        case _: return 0

print(calc(10, 15, '+')) # 25
print(calc(10, 15, '*')) # 150

# Una característica de las funciones anidadas es que conocen el contexto de la
# función en la que fueron creadas. Es decir, que las funciones anidadas pueden
# usar las variables y argumentos definidos en la función contenedora. Ejemplo:

def goodbye(name: str = 'desconocido') -> None:
    # Esta función interna usa la variable `name` definida en la función
    # `goodbye()`. Esta función interna se conoce como closure.
    def say_goodbye():
        return f'Adiós {name}'
    return say_goodbye()

# Y podemos ver que funciona.
print(goodbye('Luiggi')) # 'Adiós Luiggi'. Internamente usa el closure `say_goodbye()`

# En nuestras funciones devolvemos un objeto que posee un valor calculado. ¿Qué
# pasaría si el objeto de retorno fuese una función, o específicamente, un
# closure?

# Lo probaremos con una función que devolverá otra función para elevar un
# número `x` a una `n`-ésima potencia conocida.
# Nuestro argumento inicial será `n`, que define la `n`-ésima potencia.
def powerTo(n: int):
    # `powerToN` es un closure que recibe un número `x` que será elevado a la
    # `n`-ésima potencia.
    def powerToN(x: int) -> int:
        return x ** n
    return powerToN # Nótese que aquí devolvemos la función *sin paréntesis*

# `squared(x)` es una función basada en `powerToN(x)`. Significa que
# `squared(x)` podrá elevar cualquier número a la 2 (al cuadrado).
squared = powerTo(2)
print(squared(5)) # 25
print(squared(9)) # 81

# Los closures son una herramienta poderosa que permite generar funciones con
# contexto a partir de otras funciones. La función contenedora se convierte en
# una fábrica de closures, y tiene la responsabilidad de proveer el contexto
# apropiado. El closure provee un mecanismo para ejecutar una operación solo
# cuando sea solicitado.

# Otros usos de `powerTo`.
cubed = powerTo(3) # La `n`-ésima potencia será 3.
print(cubed(3)) # 27
print(cubed(10)) # 1000

fourth_power = powerTo(4) # La `n`-ésima potencia será 4.
print(fourth_power(4)) # 256
print(fourth_power(20)) # 160000

# `powerTo(5)` returns the closure, and then we immediately invoke it on 2.
print(powerTo(5)(2)) # 32

# Veamos un ejemplo más cercano a código real.
# Tenemos una función que recibe un párrafo, la separa por puntos, capitaliza
# cada oración y vuelve a unir todo el contenido en una cadena.
def capitalize(paragraph: str) -> str:
    sentences = paragraph.split('.')
    result = []
    for sentence in sentences:
        result.append(sentence.strip().capitalize())
    return ". ".join(result)

# Por otro lado, tenemos una función que recibe una cadena y le pone un borde
# de `#`, como los títulos de las lecciones.
def borderize(s: str) -> str:
    lines = s.split('\n')
    longest_line_size = 0
    for line in lines:
        longest_line_size = len(line) if len(line) > longest_line_size \
            else longest_line_size
    bordered_lines = []
    for line in lines:
        bordered_lines.append(f'#  {line:{longest_line_size}}  #')
    border = '#' * (longest_line_size + 6)
    all_lines = "\n".join(bordered_lines)
    return f'{border}\n{all_lines}\n{border}'

# Podemos generar una función `decorate()` que aplique ambas funcionalidades
# sobre una cadena. La característica principal es que esta función no conoce de
# las funciones `capitalize()` ni `borderize()`.
def decorate(str_func1, str_func2):
    return lambda s: str_func2(str_func1(s))

borderize_and_capizalize = decorate(capitalize, borderize)
print(borderize_and_capizalize('hola. esta es una lección. espero que te guste'))
'''
Salida:
####################################################
#  Hola. Esta es una lección. Espero que te guste  #
####################################################
'''

# Esto nos permite, por ejemplo, tener una tercera función que separe un párrafo
# en múltiples líneas de longitud mayor o igual a 70 caracteres.
def left_indent(paragraph: str, size:int = 70) -> str:
    if (len(paragraph) <= size): return paragraph
    words = paragraph.split()
    lines = []
    new_line = ''
    for word in words:
        if len(new_line) + len(word) >= size:
            lines.append(new_line)
            new_line = ''
        new_line = f'{new_line} {word}'
    lines.append(new_line)
    return '\n'.join(lines)

# Y podemos generar una nueva función que capitalize las oraciones, divida el
# párrafo en múltiples líneas y ponga un borde al párrafo.
# ¡Cuidado! El orden de las funciones a decorar es importante.
beautify = decorate(decorate(capitalize, left_indent), borderize)
paragraph = '''hola y bienvenidos a la segunda lección de funciones. esta vez revisaremos
high order functions, lambdas y closures. estos 3 son mecanismos relevantes
usados en la programación funcional. personalmente, me gusta ver cómo el
lenguaje soporta de forma natural la programación funcional y ofrece las
herramientas para aplicar este paradigma.
'''
print(beautify(paragraph))
'''
Salida:
############################################################################
#   Hola y bienvenidos a la segunda lección de funciones. Esta vez         #
#   revisaremos high order functions, lambdas y closures. Estos 3 son      #
#   mecanismos relevantes usados en la programación funcional.             #
#   Personalmente, me gusta ver cómo el lenguaje soporta de forma natural  #
#   la programación funcional y ofrece las herramientas para aplicar este  #
#   paradigma.                                                             #
############################################################################
'''
