############################################
#  Estructuras de datos: Conjuntos (Sets)  #
############################################

# El conjunto es una estructura de datos mutable que posee elementos únicos.
# No permite agregar elementos repetidos. Simula un conjunto matemático.
#
# Características de los conjuntos:
# - Los elementos a agregar se evalúan para comprobar que no existen.
# - Los elementos existentes no poseen un orden específico*.
# - Permite eliminar elementos.
# - No permite actualizar elementos.
#
# * Posiblemente sí haya una definición para el orden. Pero lo omitiré por ahora.
#
# Análisis asintótico de las operaciones con Big O:
#   Operación    Mejor   Promedio   Peor
# -----------------------------------------
#   Inserción:    O(1)     O(1)     O(N)
#   Acceso:       O(1)     O(1)     O(N)
#   Búsqueda:     O(1)     O(1)     O(1)
#   Eliminación:  O(1)     O(1)     O(N)
#

print('Creación de conjunto:')

# El conjunto se crea con la función `set()`.
my_set = set() # Conjunto vacío.
print(my_set) # set()
print(type(my_set)) # <class 'set'>
# Se puede crear un conjunto pre populado usando llaves. Los elementos deben
# separarse por `,`.
my_set = { 'Jorge', 'Diana', 'Gerson', 'Jorge', 'Eddy', 'Carla', 'Gerson', 'Jorge' }
# Los elementos duplicados no serán agregados.
print(my_set) # El resultado varía con cada ejecución del programa.
print(type(my_set)) # <class 'set'>

print('---------------------------------------------------------------------')
print('Agregar elementos:')

# Se agregan elementos con la función `add()`.
my_set.add('Luiggi')
print(my_set) # El resultado varía con cada ejecución del programa.
# Si intentamos agregar un elemento duplicado, el conjunto no lo agregará.
my_set.add('Luiggi') # ¿Otra vez este señor?
print(my_set) # El resultado varía con cada ejecución del programa.

print('---------------------------------------------------------------------')
print('Acceso a elementos:')

# No se puede acceder a los elementos de un conjunto. En su lugar, solo podemos
# evaluar si un elemento existe en el conjunto con el operador `in`.

print('Marta' in my_set) # False
print('Diana' in my_set) # True

print('---------------------------------------------------------------------')
print('Concatenación de conjuntos:')

# Para concatenar dos conjuntos, se puede usar el operador `|`.
first_set = { 1, 2, 3, 4, 5}
second_set = { 2, 4, 6, 8 }
result = first_set | second_set
print(result) # {1, 2, 3, 4, 5, 6, 8}

# También podemos adicionar los contenidos en un conjunto existente con la
# función `update()`. 
numbers = { -2, -1, 0 }
pos_numbers = { 1, 2, 3 }
numbers.update(pos_numbers)
print(numbers) # {0, 1, 2, 3, -1, -2}

print('---------------------------------------------------------------------')
print('Eliminar elementos:')

# Se puede usar la función `pop()` para eliminar el último elemento del conjunto
# y obtener el elemento eliminado. Sin embargo, este resultado puede variar por
# ejecución del programa, así que debe usarse con cuidado.

elem = my_set.pop()
print(my_set) # El resultado varía con cada ejecución del programa.
print(elem) # El resultado varía con cada ejecución del programa.

my_set = { 'Jorge', 'Diana', 'Gerson', 'Jorge', 'Eddy', 'Carla', 'Luiggi' }
# Las funciones `remove()` y `discard()` permiten remover elementos. La
# diferencia entre ellas es que `remove()` lanzará un error si el elemento a
# remover no existe.
my_set.remove('Jorge')
print(my_set)
# my_set.remove('Jorge') # KeyError: 'Jorge'
my_set.discard('Luiggi')
print(my_set)
my_set.discard('Luiggi') # No da error.
print(my_set)

# Se pueden eliminar todos los elementos del conjunto con la función `clear()`.
my_set.clear()
print(my_set) # set()

print('---------------------------------------------------------------------')
print('Operaciones particulares de conjuntos:')

# Diferencia de conjuntos

# Se pueden realizar las operaciones de unión (`|`), diferencia (`-`),
# intersección (`&`) y diferencia simétrica (`^`) entre dos conjuntos.
# Cabe resaltar que todas estas operaciones no modifican ninguno de los
# conjuntos, en su lugar crean un nuevo conjunto.
# También existen funciones en lugar de usar los operadores.

math_students = { 'Maria', 'Juan', 'Roberto', 'Elsa', 'Andrea' }
statistics_students = { 'Carlos', 'Daniela', 'Andrea', 'Juan', 'Omar' }

# Alumnos que estudian matemáticas y estadística.
# Función `intersection()`.
print(math_students & statistics_students) # Intersección

# Alumnos que solo estudian matemáticas.
# Función `difference()`.
print(math_students - statistics_students) # Diferencia
# Alumnos que solo estudian estadística.
print(statistics_students - math_students) # Diferencia

# Alumnos que estudian o matemática o estadística.
# Función `symmetric_difference()`.
print(math_students ^ statistics_students) # Diferencia simétrica

# Todos los estudiantes.
# Función `union()`.
print(math_students | statistics_students) # Unión

print('---------------------------------------------------------------------')
print('Conversión a lista:')

# Todas las conversiones se realizan con la función `list()`.
my_list = list(math_students)
print(my_list) # El resultado varía con cada ejecución del programa.
