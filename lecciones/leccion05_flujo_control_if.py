#####################################################
#  Flujos de control: if-else, if-elif, match-case  #
#####################################################

# En nuestros programas debemos evaluar si ciertos datos cumplen condiciones
# y en base a eso se ejecutarán más sentencias (o no).

print('Uso de if:')
# Este flujo de control permite ejecutar sentencias solo si se cumplen
# las condiciones definidas.
# Este bloque está definido como
# if <condicion>: <- dos puntos
#     <sentencias>
# |||| <- indentado (4 caracteres)
# El indentado es la forma de ordenar código en Python
# Si las sentencias no están indentadas, no formarán parte del bloque `if`.

my_age = 20
cousin_age = 12
aunt_age = 43
if my_age > cousin_age:
    print('Soy mayor que mi primo')
# Se puede colocar más de 1 sentencia dentro del if
if my_age < aunt_age:
    print('Soy menor que mi tía')
    print('Espero que no me pida cuidar a mi sobrino =_=')

# Las condiciones siempre evalúan a un valor `bool` (`True` o `False`).
# Esto significa que podemos usar variables `bool` como condición:
live_with_parents = True
if live_with_parents == True: # No es necesario validar si un `bool` es `True`
    print('Vivo con mis padres')
if live_with_parents: # No es necesario validar si un `bool` es `True`
    print('Así es. Vivo con mis padres')

# Se pueden colocar más condicionales dentro del `if`.
# Estas se pueden relacionar con `and`
# `and` indica que ambas condiciones deben cumplirse (ser `True`)
if my_age > cousin_age and my_age < aunt_age:
    print('Mi tía me puede pedir que cuide a mi primo u_uU')

# También se pueden relacionar con `or`
# `or` indica que basta que una condición sea `True` para que todo sea `True`.
if cousin_age > aunt_age or cousin_age < my_age:
    # La primera condición es un absurdo, solo para demostrar
    # que si la segunda es `True`, entrará al `if`.
    print('Mi primo es menor que yo y menor que mi tía')

print('---------------------------------------------------------------------')
print('if anidados:')
# Las sentencias `if` se pueden anidar. Es decir, colocar una dentro de otra.
if my_age > cousin_age:
    print('Soy mayor que mi primo')
    if live_with_parents:
        # Esta sentencia necesita doble identado para indicar que se ejecutará
        # dentro del segundo `if`.
        print('Genial, me quedaré en casa con mi primo')
    # Las sentencias con 1 solo identado se ejecutarán solo con el primer `if`
    print('Ajá, soy mayor que mi primo. Lo mencioné antes.')

print('---------------------------------------------------------------------')
print('Uso de if-else:')

# La sentencia `else` permite ejecutar sentencias en caso que no se hayan
# cumplido las condiciones del `if`.

if my_age < 18:
    print('Soy menor de edad')
else:
    print('Soy mayor de edad. ¡Yay!')

# `else` también permite anidación de sentencias `if`.

brother_age = 29
if my_age > brother_age:
    print('Soy el hermano mayor')
else:
    print('Soy el hermano menor')
    if my_age >= 18:
        print('Mi hermano y yo somos mayores de edad')

print('---------------------------------------------------------------------')
print('Uso de elif:')
# Puede suceder que debamos anidar if-else múltiples veces
color = 'Rojo'
if color == 'Verde':
    print('Elegiste el color Verde')
else:
    if color == 'Azul':
        print('Elegiste el color Azul')
    else:
        if color == 'Amarillo':
            print('Elegiste el color Amarillo')
        else:
            if color == 'Rojo':
                print('Elegiste el color Rojo')
            else:
                print('No sé qué color elegiste')

# Python provee la sentencia `elif` para facilitar la escritura de if-else anidados.
color = 'Morado'
if color == 'Verde':
    print('Elegiste el color Verde')
elif color == 'Azul':
    print('Elegiste el color Azul')
elif color == 'Amarillo':
    print('Elegiste el color Amarillo')
elif color == 'Rojo':
    print('Elegiste el color Rojo')
else:
    print('No sé qué color elegiste')

print('---------------------------------------------------------------------')
print('Uso de match:')

# Otra forma de escribir múltiples casos para una condición es
# mediante `match` y `case`. Estos están disponibles desde Python 3.10

color = 'Azul'
match color:
    case 'Verde':
        print('Elegiste el color Verde')
    case 'Azul':
        print('Elegiste el color Azul')
    case 'Amarillo':
        print('Elegiste el color Amarillo')
    case 'Rojo':
        print('Elegiste el color Rojo')
    case _: # En caso que no se cumplió ninguna condición.
        print('No sé qué color elegiste')

# También funciona con números
comando = 3
match comando:
    case 1:
        print('Comando con código 1')
    case 2:
        print('Comando con código 2')
    case 3:
        print('Comando con código 3')
    case _:
        print('Comando con código desconocido')

comando = '??'
match comando:
    case 1:
        print('Comando con código 1')
    case 2:
        print('Comando con código 2')
    case 3:
        print('Comando con código 3')
    case _:
        print('Comando con código desconocido')

print('---------------------------------------------------------------------')
print('Operador ternario:')
# El operator "ternario" sirve para asignar el valor a una variable.
# En otros lenguajes, este operador se compone así:
# variable = <condición> ? <resultado_si_cumple> : <resultado_no_cumple>
# En Python, es un poco más textual:
# variable = <valor> if <condiciones> else <otro_valor>
# En mi opinión, facilita la lectura del código por ser más humano.

country = 'US'
legal_age = 21 if country == 'US' else 18
print(legal_age) # 21

# Este operador solo sirve para asignar variables
legal_age = 18 if country != 'US' else print('No soy mayor de edad')
print(legal_age) # None (!?!?!?) Esto lo revisaremos en detalle más adelante

# El operador ternario también admite if-else anidados
age_compare = 1 if my_age > brother_age else (0 if my_age == brother_age else -1)
print(age_compare) # -1

print('---------------------------------------------------------------------')
print('Valores "Truthy" y "Falsy":')
# Las condiciones siempre convergen en un valor lógico `bool`: `True` o `False`.
# Esto lo revisamos en las líneas 28 a 34.
# Sin embargo, debido a que Python tiene tipado dinámico, hay variables de tipo
# `int` o `str` que pueden comportarse como `bool`. Esto se conoce como
# `Truthy` (se comporta como `True`) y `Falsy` (se comporta como `False`).

# Números (int y float):
int_falsy = 0 # número 0 es Falsy
if int_falsy:
    print(f'Soy número truthy: {int_falsy}')
else:
    print(f'Soy número falsy: {int_falsy}')
int_falsy = -0 # número -0 es Falsy
if int_falsy:
    print(f'Soy número truthy: {int_falsy}')
else:
    print(f'Soy número falsy: {int_falsy}')
int_truthy = 1 # Cualquier número distinto de 0 es Truthy
if int_truthy:
    print(f'Soy número truthy: {int_truthy}')
else:
    print(f'Soy número falsy: {int_truthy}')
int_truthy = -1
if int_truthy:
    print(f'Soy número truthy: {int_truthy}')
else:
    print(f'Soy número falsy: {int_truthy}')
int_truthy = 0.5
if int_truthy:
    print(f'Soy número truthy: {int_truthy}')
else:
    print(f'Soy número falsy: {int_truthy}')

# Cadenas
str_falsy = '' # cadena vacía es Falsy
if str_falsy:
    print(f'Soy cadena truthy: [{str_falsy}]')
else:
    print(f'Soy cadena falsy: [{str_falsy}]')
str_truthy = ' ' # cadena con cualquier contenido es Truthy
if str_truthy:
    print(f'Soy cadena truthy: [{str_truthy}]')
else:
    print(f'Soy cadena falsy: [{str_truthy}]')

# Listas
list_falsy = [] # lista vacía es Falsy
if list_falsy:
    print(f'Soy lista truthy: {list_falsy}')
else:
    print(f'Soy lista falsy: {list_falsy}')
list_truthy = [0] # lista con al menos 1 elemento es Falsy
if list_truthy:
    print(f'Soy lista truthy: {list_truthy}')
else:
    print(f'Soy lista falsy: {list_truthy}')
