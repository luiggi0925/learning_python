###########################
#  Comprensión de listas  #
###########################

# La comprensión de listas (list comprehension en inglés) es el mecanismo de
# Python para facilitar la manipulación de colecciones (listas, diccionarios,
# conjuntos, etc).

# La forma general de la comprensión de listas es
#
#     variable for variable in iterable [if <condiciones>]
#
# La parte `[if <condiciones>]` es opcional.

# Veremos algunos ejemplos de cómo facilitar la escritura de código.

print('Inicio:')

numbers = list(range(-10, 11))
# Obtener una copia de la lista.
numbers_copy = list(i for i in numbers)
print(numbers_copy) # [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

# Obtener los números pares de una lista.
pairs = []
for number in numbers:
    if number % 2 == 0:
        pairs.append(number)
print(pairs) # [-10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10]
# Esto tomó 4 líneas de código. Con comprensión de listas lo podemos hacer en 1.
pairs = list(n for n in numbers if n % 2 == 0)
print(pairs) # [-10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10]

# Obtener los cuadrados de los múltiplos de 3.
squared_3multipliers = set(n * n for n in numbers if n % 3 == 0)
print(squared_3multipliers) # {0, 81, 36, 9}

# Probemos reescribir esta función `print_square()` de la lección 11.
'''
Previamente:
def print_square(n: int = 8) -> None:
    horizontal = ''
    for i in range(n):
        horizontal += '* '
    print(horizontal)
    for i in range(n-2):
        line = '* '
        for j in range(n-2):
            line += '  '
        line += '*'
        print(line)
    print(horizontal)
'''
# Ahora usando comprensión de listas.
def print_square(n: int = 8) -> None:
    print('* ' * n)
    print('\n'.join(f"* {'  ' * (n-2)}*" for i in range(n-2) )) 
    print('* ' * n)

print_square(5)
'''
* * * * *
*       *
*       *
*       *
* * * * *
'''

print('---------------------------------------------------------------------')
print('Conversiones entre colecciones:')

# La comprensión de listas también permite recolectar datos hacia otras
# colecciones.

matrix = [[1,2,3], [4,5,6], [7,8,9]]
# Queremos obtener una lista con el contenido de todas las listas internas de
# nuestra matriz.
# Solución sin comprensión de listas.
flattened_list = []
for l in matrix:
    for n in l:
        flattened_list.append(n)
print(flattened_list) # [1, 2, 3, 4, 5, 6, 7, 8, 9]

# Solución con comprensión de listas.
# Primero recorremos las listas en la matriz. Luego recorremos cada lista. El
# valor a devolver será cada número. El resultado devuelto será la entrada para
# completar nuestra lista.
flattened_list = [i for l in matrix for i in l]
print(flattened_list) # [1, 2, 3, 4, 5, 6, 7, 8, 9]

# Convertir una lista de tuplas en un diccionario.
students = [('Luiggi', 19), ('Jorge', 17), ('María', 20), ('Andrea', 18) ]
students_with_ages = { name:age for name,age in students } # Podemos usar desempaquetado aquí.
print(students_with_ages) # {'Luiggi': 19, 'Jorge': 17, 'María': 20, 'Andrea': 18}

students = [('Luiggi', 19), ('Jorge', 17), ('María', 20), ('Andrea', 18) ]

# Obtener solo las edades de los estudiantes.
student_ages = [age for _,age in students] 
print(student_ages) # [19, 17, 20, 18]

print('---------------------------------------------------------------------')
print('Rendimiento:')

# El rendimiento de las comprensiones de listas dependerá del iterable que se
# está iterando y de las operaciones que realicemos dentro de ellos.

# Todos los ejemplos anteriores proveen una versión sin y con comprensión de
# listas, donde la comprensión de lista hace exactamente lo mismo que la versión
# sin ella. Existen casos donde la comprensión de listas requiere de operaciones
# adicionales para obtener el resultado esperado.

# Por ejemplo. Tenemos las edades de los alumnos de un curso virtual.
student_ages = [17, 19, 17, 20, 25, 17, 25, 16, 20, 18, 19, 18, 17, 25, 18]
# Queremos saber cuántos alumnos existen por edad.
# Podemos usar un diccionario donde la llave sea la edad y el valor sea la
# cantidad de alumnos.
# Solución sin comprensión de listas.
student_ages_counter = {}
for age in student_ages:
    student_ages_counter[age] = student_ages_counter.get(age, 0) + 1
print(student_ages_counter) # {17: 4, 19: 2, 20: 2, 25: 3, 16: 1, 18: 3}
# Solución con comprensión de listas.
# Esta solución es corta pero conlleva a problemas de rendimiento.
student_ages_counter = { age:student_ages.count(age) for age in student_ages }
print(student_ages_counter) # {17: 4, 19: 2, 20: 2, 25: 3, 16: 1, 18: 3}

# Esta comprensión de listas equivale a:
student_ages_counter = {}
for age in student_ages:
    student_ages[age] = student_ages.count(age)
# Esto tiene los siguientes problemas:
#
# 1. Cada invocación a `student_ages.count(age)` va a iterar sobre toda la
#    lista, generando así una solución O(N^2).
# 2. Adicionalmente, estamos insertando múltiples veces llaves que existen en el
#    diccionario. Por ejemplo, la segunda vez que `age` sea 17, volverá a
#    ejecutar lo mismo que la primera vez, generando un costo innecesario.

# Una optimización puede ser convertir la lista en conjunto para iterar solo
# sobre las edades. Esto reduce la cantidad de invocaciones a `count()` y
# resuelve el problema 2. Sin embargo, se penaliza el espacio y tiempo para
# crear el conjunto a partir de la lista.
student_ages_counter = { age:student_ages.count(age) for age in set(student_ages) }
print(student_ages_counter) # {16: 1, 17: 4, 18: 3, 19: 2, 20: 2, 25: 3}

# En los casos iniciales, donde la comprensión de listas hace exactamente lo
# mismo que los bucles for, conviene más usar la comprensión de listas. En caso
# que la comprensión de listas vaya en detrimento del rendimiento de la
# aplicación, lo mejor será usar las vías tradicionales.

# En conclusión, la comprensión de listas es una herramienta poderosa. Siempre
# será mejor usar el código simple y más efectivo (óptimo) por sobre nuestras
# preferencias personales.
