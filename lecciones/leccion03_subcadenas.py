################
#  Subcadenas  #
################

# Una subcadena es un fragmento de una cadena.

# Para los ejemplos, obtenemos una cadena con dígitos del 0 al 9
digit_string = '01234567890123456789'
char_only_string = 'abcdefghijklmnopqrstuvwxyz'

print('Subcadenas de 1 caracter:')
# Para obtener un caracter de una cadena, se accede por índice.
# Todas las cadenas empiezan en el índice 0 y terminan en el índice `len(cadena) - 1`.
print(digit_string[0]) # '0'
print(digit_string[1]) # '1'
print(char_only_string[0]) # 'a'
print(char_only_string[1]) # 'b'
# Solo existe el tipo str. Un caracter es una cadena de longitud 1.
print(type(digit_string[1])) # <class 'str'>
print(len(digit_string[1])) # 1

print('---------------------------------------------------')
print('Subcadenas de rango:')
# Obtener una subcadena a partir de los índices.
# Inicio: inclusivo. Fin: exclusivo (ignorado).
# El segundo índice es el índice de corte. No es la longitud que debe tener la subcadena.
print(digit_string[0:5]) # '01234'
print(digit_string[0:7]) # '0123456'
print(char_only_string[0:6]) # 'abcdef'
# El segundo índice es el índice de corte. No es la longitud que debe tener la subcadena.
print(digit_string[2:7]) # '23456'
print(char_only_string[5:14]) # 'fghijklmn'

print('---------------------------------------------------')
print('Subcadenas con índices negativos:')
# Los valores de los índices pueden ser negativos. Python hará el cálculo entre
# la longitud de la cadena menos el índice.
print(len(digit_string)) # 21
print(digit_string[-1]) # '9' porque es el último caracter
print(digit_string[-2]) # '8' porque es el penúltimo caracter
print(len(char_only_string)) # 26
print(char_only_string[-1]) # z
print(char_only_string[-2]) # y

print('---------------------------------------------------')
print('Subcadenas con 1 solo índice (el otro con valor por defecto):')
# Los índices tienen valores por defecto. Python reconocerá los valores apropiadamente.
# Por lo general, Inicio por defecto es 0 y Fin por defecto es `len(cadena) - 1`.
# Esto permite generar subcadenas desde un índice hasta el final
# o desde el inicio hasta un índice conocido.
print(digit_string[17:]) # '789'
print(digit_string[:8]) # '01234567'
print(char_only_string[23:]) # 'xyz'
print(char_only_string[:5]) # 'abcde'
# También se puede usar con los valores negativos.
print(digit_string[-8:]) # '23456789'. Similar a digit_string[12:]
print(digit_string[:-17]) # '012'. Similar a digit_string[:3]
print(char_only_string[-5:]) # 'uvwxyz'. Similar a char_only_string[21:]
print(char_only_string[:-23]) # 'abcde'. Similar a char_only_string[:3]

print('---------------------------------------------------')
print('Subcadenas de rango con índices negativos:')
# Asimismo, se pueden usar índices negativos para Inicio y Fin.
print(digit_string[-19:-16]) # '123'. Similar a digit_string[1:4]
print(char_only_string[-20:-16]) # 'ghij'. Similar a char_only_string[6:10]

print('---------------------------------------------------')
print('Subcadenas usando el paso:')
# Existe un tercer argumento: el Paso (step). Para construir la subcadena:
# 1. Se empieza por el índice Inicio. Este será el índice actual.
# 2. Se obtiene el caracter del índice actual.
# 3. Se suma la cantidad de Paso al índice actual.
# 4. Si el índice actual es igual o superior al índice Fin, detener.
# 5. Repetir 2.

# Ejemplo: `digit_string[1:20:4]`
# Empieza en índice Inicio (1). Se obtiene digit_string[1] ('1').
# Se avanza conforme a la cantidad del Paso (4).
# El siguiente índice sería 5. Se agrega digit_string[5] ('5').
# Se avanza conforme a la cantidad del Paso (4).
# El siguiente índice sería 9. Se agrega digit_string[9] ('9').
# Esto se repetirá hasta llegar al índice 20.
# Como es superior o igual al índice Fin (20), se detiene.
# El resultado sería 15937
print(digit_string[1:20:4]) # 15937
print(char_only_string[3:18:2]) # 'dfhjlnpr'
# El valor por defecto del Paso es 1
print(digit_string[2:6:1]) # 2345
print(char_only_string[0:5:1]) # 'abcde'

# El Paso no puede ser 0. La siguiente línea lanza un error.
# print(long_string[0:7:0]) # ValueError: slice step cannot be zero

print('---------------------------------------------------')
print('Subcadenas usando el paso negativo:')
# El Paso también puede ser negativo. Significa que al recolectar los caracteres,
# en lugar de avanzar va a retroceder.
# Ejemplo: invertir una cadena.
print(digit_string[::-1]) # '98765432109876543210'
print(char_only_string[::-1]) # 'zyxwvutsrqponmlkjihgfedcba'
# print(long_string[-1:-10]) # yields ''. Again, be careful.


# ¡Cuidado! Si el Paso es negativo, Inicio debe ser *menor* a Fin.
# De lo contrario se generará una cadena vacía.
print(digit_string[10:12:-1]) # '', cadena vacía.
print(digit_string[12:10:-1]) # '21'.
print(digit_string[15:8:-2]) # '5319'.
