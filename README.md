# Lecciones de Python

Este proyecto tiene mi introducción y aprendizaje por el mundo de Python.

Conforme tenga más lecciones y ejercicios, los plasmaré en este repositorio.

## Table of Contents

1. [Tech stack](#tech-stack)
1. [Cómo usar](#cómo-usar)
1. [Autor](#autor)

## Tech stack

- Python >3.10
- Pytest

## Cómo usar

- Clonar el repositorio.
- Crea el ambiente virtual: `$ python -m pip install`
- Ejecuta las pruebas: `$ pytest`

A considerar:

- La carpeta [lecciones](/lecciones/) posee mis notas de aprendizaje. Ejecuta el o los módulos de tu preferencia, espero que las notas te apoyen como a mí.
- La carpeta [ejercicios](/ejercicios/) posee algunos ejercicios que me he puesto a modo de practicar las características aprendidas. También posee mis soluciones y un conjunto de pruebas unitarias para comprobar el funcionamiento solicitado.

## Autor

Aloha! Soy Luiggi, el autor de este repositorio. Espero que mis notas y ejercicios te sirvan tanto como lo hicieron conmigo. He tratado de colocar ejemplos y escenarios que encuentro relevantes para mi aprendizaje y por mi curiosidad de explotar las capacidades del lenguaje.

Puedes contactar conmigo en las siguientes plataformas:

- StackOverflow: [Luiggi Mendoza](https://stackoverflow.com/users/1065197/luiggi-mendoza)
- Twitter: [@lithoxd](https://twitter.com/lithoxd)
- LinkedIn: [luiggimendoza](https://www.linkedin.com/in/luiggimendoza/)
- Aquí mismo. Déjame un mensaje y lo atenderé a la brevedad posible.
