class StoreEmptyEventError(BaseException):
    'You tried to store an empty event.'

class ShutteddownEventQueueManager(BaseException):
    'Event Queue Manager is shutdown. No operations are available.'
