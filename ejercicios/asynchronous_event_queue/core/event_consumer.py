from abc import abstractmethod, ABC
from .event import Event

class EventConsumer(ABC):
    @abstractmethod
    def consume(self, event: Event):
        pass

