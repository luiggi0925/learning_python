from typing import Any, Callable, Tuple

class EventProducer:
    def __init__(self, target: Callable[[Tuple], None] = None, *args) -> None:
        pass