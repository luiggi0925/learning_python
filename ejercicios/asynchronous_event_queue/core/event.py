from dataclasses import dataclass
from typing import Dict, Generic, TypeVar

T = TypeVar('T')

@dataclass(frozen=True)
class Event(Generic[T]):
    body: T
    headers: Dict
