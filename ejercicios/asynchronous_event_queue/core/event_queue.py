from queue import Queue
from typing import Dict, TypeVar, overload

from .event import Event

T = TypeVar('T')

class EventQueue:
    def __init__(self) -> None:
        self.queue = Queue()
    def store(self, event: Event) -> None:
        self.queue.put(event)
    def retrieve(self) -> Event:
        return self.queue.get()
    def processed_after_retrieve(self) -> None:
        self.queue.task_done()
    @property
    def len(self):
        return len(self.queue)
