from queue import Queue
from threading import Thread
from typing import Callable, Dict, TypeVar, overload, List
import collections
from .event_consumer import EventConsumer
from .event import Event
from .errors import StoreEmptyEventError, ShutteddownEventQueueManager

T = TypeVar('T')

class _EventQueueData:
    def __init__(self, name: str, queue: Queue = None,\
        # producers: List[EventConsumer | Callable[[Event], None]] = [],\
        consumers: List[EventConsumer | Callable[[Event], None]] = []) -> None:
        self._name = name
        self._queue = queue if queue else Queue()
        # self._producers = [*producers]
        self._consumers = [*consumers]
    def __on_store_to_void(self, event):
        pass
    def store(self, event: Event) -> None:
        self._queue.put(event)
    def has_pending_tasks(self) -> bool:
        return self._queue.unfinished_tasks > 0 and len(self._consumers) > 0
    def send_to_consumers(self, block: bool = False) -> None:
        event = self._queue.get(block=block)
        for consumer in self._consumers:
            try:
                match consumer:
                    case EventConsumer(): consumer.consume(event)
                    case collections.abc.Callable(): consumer(event)
                    case _: self.__on_store_to_void(event)
            except Exception as e:
                print('Error consuming the event', e)
        self._queue.task_done()

class EventQueueManager(Thread):
    def __init__(self, queue_names: List[str] = [], daemon: bool = True) -> None:
        super().__init__(name='EventQueueManagerDaemonThread', daemon=daemon)
        self.__queues = { '': _EventQueueData('') }
        self.__queues.update({ name: _EventQueueData(name) for name in queue_names})
        self.__alive = True
        self.start()
    def create_queue(self, name: str) -> None:
        self.__validate_alive()
        self.__queues[name] = _EventQueueData(name)
    @overload
    def store(self, content: T, headers: Dict = {}, queue: str = '') -> None:
        ...
    @overload
    def store(self, event: Event, queue: str = '') -> None:
        ...
    def store(self, event: Event = None, content: T = None, headers: Dict = {}, queue: str = '') -> None:
        self.__validate_alive()
        if event:
            self.__queues[queue].store(event)
        elif content:
            self.__queues[queue].store(Event(content, headers))
        else:
            raise StoreEmptyEventError()
    def subscribe(self, subscriber: EventConsumer | Callable[[Event], None],\
        queue: str = '') -> None:
        self.__validate_alive()
        self.__queues[queue]._consumers.append(subscriber)
    def run(self) -> None:
        while self.__alive:
            for _, queue in tuple(self.__queues.items()):
                while queue.has_pending_tasks():
                    queue.send_to_consumers()
    def shutdown(self) -> None:
        self.__alive = False
    def __validate_alive(self) -> None:
        if not self.__alive: raise ShutteddownEventQueueManager()
