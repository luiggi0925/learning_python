'''
Ejercicio: Cola de eventos asíncrona.

1. Crear un componente de cola de eventos que permita recibir y almacenar
   eventos de forma asíncrona.
2. Agregar funcionalidad a la cola de eventos para que permita enviar los
   eventos a uno o más destinatarios de forma asíncrona.
3. Crear un gestor de múltiples colas de eventos. Cada cola que maneje tendrá
   un nombre para ser identificada.
4. Crear productores que envíen eventos al gestor de eventos.
5. Crear consumidores de que procesen los eventos recibidos del gestor de
   eventos.
6. Crear un agente que puede emitir los siguientes comandos al gestor de
   eventos:
   - CREATE(queue_name): Crea una nueva cola de eventos.
   - LOOKUP(queue_name): Permite mirar los eventos sin consumir almacenados en
     una cola.
   - REMOVE(queue_name): Elimina una cola. Todos los productores de la cola
     recibirán un error al intentar producir eventos. Los consumidores podrían
     recibir un error indicando que no recibirán más eventos.
'''

from time import sleep
from core import EventQueueManager
from core import EventConsumer
from core import Event
from core import ShutteddownEventQueueManager

class MyEventConsumer(EventConsumer):
   def consume(self, event: Event) -> None:
      print(f'Event arrived to object with body "{event.body}"')
   def __str__(self) -> str:
      return f"<class 'MyEventConsumer'>"
   def __repr__(self) -> str:
      return f"<class 'MyEventConsumer'>"

def subscriberF(event: Event) -> None:
   print(f'Event arrived to function with body "{event.body}"')

eqm = EventQueueManager(daemon=False)
eqm.create_queue('hello')
eqm.create_queue('world')

myEventConsumer = MyEventConsumer()
eqm.subscribe(myEventConsumer, 'hello')
eqm.subscribe(myEventConsumer, 'world')

eqm.subscribe(subscriberF, 'hello')
eqm.store(content='something hello', queue='hello')
eqm.store(content='something world', queue='world')

eqm.create_queue('no_consumers')
eqm.store(content='will I be consumed someday?', queue='no_consumers')
eqm.store(content='may I join you?', queue='no_consumers')

sleep(2)

eqm.shutdown()
try:
   eqm.store(content='I wanna join too')
except ShutteddownEventQueueManager:
   print('Ooops! Sorry')
