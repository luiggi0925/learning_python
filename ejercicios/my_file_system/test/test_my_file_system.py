# test with pytest
from ejercicios.my_file_system.core.my_file_metadata import MyFileMetadata
from ejercicios.my_file_system.my_file_system import MyFileSystem

class Printer:
    def __init__(self) -> None:
        self.contents = []
    def collect(self, s: str) -> None:
        self.contents.append(s)
    def toString(self) -> str:
        return "|".join(self.contents)

def test_creation():
    fs = MyFileSystem()
    printer = Printer()
    fs.show_current(printer.collect)
    assert printer.toString() == "Current: /"

def test_mkdir():
    fs = MyFileSystem()
    printer = Printer()
    fs.mkdir('sample1')
    fs.show_current(printer.collect)
    assert printer.toString() == "Current: /|sample1/"

def test_mkdir_2dirs():
    fs = MyFileSystem()
    printer = Printer()
    fs.mkdir('sample1')
    fs.mkdir('sample2')
    fs.show_current(printer.collect)
    assert printer.toString() == "Current: /|sample1/|sample2/"


def test_create_file():
    fs = MyFileSystem()
    printer = Printer()
    fs.create('sample.txt', MyFileMetadata(size= 10))
    fs.show_current(printer.collect)
    assert printer.toString() == "Current: /|sample.txt"
