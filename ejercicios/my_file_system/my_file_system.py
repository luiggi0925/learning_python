from typing import Callable

from core.my_file import MyFile
from core.my_file_metadata import MyFileMetadata
from exceptions.invalid_file_name import InvalidFileName

class MyFileSystem:
    def __init__(self):
        self.__root = MyFile(name='', is_directory=True, parent=None)
        self.__current = self.__root
    def exists(self, filename: str) -> bool:
        return self.__current.contains(filename)
    def mkdir(self, name: str, create_dir: bool = False) -> bool:
        unallowed_chars = ['?','\\','!']
        if any(unallowed in name for unallowed in unallowed_chars): raise InvalidFileName
        if name.endswith('/'): name = name[:-1]
        return self.__current.create(name, create_dir=create_dir, is_directory=True)
    def create(self, name: str, metadata: MyFileMetadata = MyFileMetadata()) -> bool:
        unallowed_chars = ['?','/','\\','!']
        if any(unallowed in name for unallowed in unallowed_chars): raise InvalidFileName
        return self.__current.create(name, metadata, is_directory=False)
    def navigate(self, dir_name: str) -> None:
        if not dir_name: return
        if dir_name == '.': return
        if dir_name == '..':
            if self.__current == self.__root: return
            self.__current = self.__current.parent
            return
        dirs = dir_name.split('/')
        cur_dir = self.__current
        i = 0
        while i < len(dirs):
            dir = cur_dir.find(dirs[i])
            if dir is None: return
            i += 1
            cur_dir = dir
        if not cur_dir.files: return
        self.__current = cur_dir
    def show_current(self, show: Callable[[str], None] = print) -> None:
        show(f"Current: {self.__current.show_name()}")
        if self.__current.files:
            [show(file.show_name()) for file in self.__current.files]
