from my_file_system import MyFileSystem

def main():
    fs = MyFileSystem()
    fs.mkdir('hello')
    fs.mkdir('world')
    fs.show_current()
    """
    fs.mkdir(name='foo/bar', create_dir=True)
    fs.navigate('foo')
    fs.create('saludo.txt', MyFileMetadata(size=5,media_type='txt'))
    fs.show_current()
    fs.navigate('.')
    fs.navigate('..')
    fs.show_current()
    """

if __name__ == "__main__":
    main()
