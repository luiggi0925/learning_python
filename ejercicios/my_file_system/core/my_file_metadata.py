from datetime import datetime
from textwrap import dedent

class MyFileMetadata:
    def __init__(self, size: int = 0, media_type: str = '',\
        read_only: bool = False, hidden: bool = False, others = {}):
        self.__size = size
        self.__media_type = media_type
        self.__read_only = read_only
        self.__hidden = hidden
        self.__others = {*others}
        self.__created_at = datetime.now()
        self.__last_updated_at = datetime.now()
    def update(self, size: int = 0, media_type: str = '',\
            read_only: bool = False, hidden: bool = False,\
            others = {}) -> None:
        self.__size = size
        self.__media_type = media_type
        self.__read_only = read_only
        self.__hidden = hidden
        self.__others.update(others)
        self.__last_updated_at = datetime.now()
        # notify
    def __str__(self) -> str:
        return dedent(f'''{{
        "created_at": {self.__created_at},
        "last_updated_at": {self.__last_updated_at},
        "size": {self.__size},
        "media_type": {self.__media_type},
        "read_only": {self.__read_only},
        "hidden": {self.__hidden},
        "other_attributes": {self.__others}
    }}
    ''')
