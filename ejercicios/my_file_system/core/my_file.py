from textwrap import dedent
from .my_file_metadata import MyFileMetadata

class MyFile:
    def __init__(self, name: str, is_directory: bool = False, metadata: MyFileMetadata = MyFileMetadata(),\
        parent = None):
        self.__name = name
        self.__is_directory = is_directory
        self.__metadata = metadata
        self.__files = []
        if not (parent is None):
            self.__parent = parent
            self.__parent.add(self)
            self.__full_name = parent.__name + '/' + self.__name
            if self.__full_name.startswith('//'): self.__full_name = self.__full_name[1:]
        else:
            self.__full_name = self.__name
    # Properties
    @property
    def name(self) -> str:
        return self.__name
    @name.setter
    def name(self, new_name: str) -> None:
        self.__name = new_name
        # notify
    @property
    def metadata(self) -> MyFileMetadata:
        return self.__metadata
    @metadata.setter
    def metadata(self, metadata) -> None:
        self.__metadata = metadata
        # notify
    @property
    def is_directory(self) -> bool:
        return self.__is_directory
    @property
    def files(self):
        return self.__files
    @property
    def full_name(self):
        return self.__full_name
    @property
    def parent(self):
        return self.__parent

    # Methods
    def __get_media_type_from_name(self, name: str):
        index = name.rfind('.')
        return name[(index+1):] if index > -1 else ''
    def add(self, file) -> None:
        self.__files.append(file)
        # notify
    def create(self, name: str, metadata: MyFileMetadata = MyFileMetadata(), is_directory = False, create_dir: bool = False) -> bool:
        if name.find('/') >= 0:
            path_splitted = name.split('/')
            i = 0
            cur = self
            while i < len(path_splitted) - 1:
                f = next((file for file in cur.__files if file.__name == path_splitted[i]), None)
                if f is not None:
                    i += 1
                    cur = f
                else:
                    if not create_dir: return False
                    f = MyFile(path_splitted[i], is_directory=True, parent=cur)
                    cur = f
                    i += 1
            filename = path_splitted[i]
            if cur.contains(filename):
                return False
            f = MyFile(filename, is_directory, metadata, cur)
            return True
        else:
            if self.contains(name):
                return False
            f = MyFile(name, is_directory, metadata, self)
            return True
        # notify
    def find(self, filename: str, recursive: bool = False):
        if self.__name == filename:
            return self
        if recursive:
            it = (file for file in self.__files if file.find(filename) is not None)
        else:
            it = (file for file in self.__files if file.name == filename)
        f = next(it, None)
        return f
    def contains(self, filename: str, recursive: bool = False) -> bool:
        return self.find(filename, recursive) is not None
    def __str__(self) -> str:
        return dedent(f'''{{
        "name": "{self.__name}",
        "full_name": "{self.__full_name}",
        "metadata": {self.__metadata}
    }}
    ''')
    def show_name(self) -> str:
        return self.__name if not self.__is_directory else self.__name + '/'
