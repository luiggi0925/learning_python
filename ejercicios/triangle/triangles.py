'''
Ejercicio: Triángulos.

1. Tener un mecanismo que permita crear un triángulo. Debe permitir recibir
   los valores de sus 3 lados.
2. Al momento de crear el triángulo, se debe evaluar que sea un triángulo válido.
   En caso que no, lanzar un error.
3. Definir una función que permita obtener el tipo de triángulo:
   - Equilátero (todos los lados iguales)
   - Isósceles (2 lados son iguales)
   - Escaleno (todos sus lados son diferentes)
4. Definir una función que permita calcular el perímetro del triángulo.
5. Definir una función que permita calcular el área de un triángulo.
6. Definir una función que permita comprobar si es un triángulo rectángulo.
'''

from enum import Enum
from functools import reduce
from math import sqrt

class InvalidTriangle(Exception):
    'Invalid triangle'

class TriangleTipe(Enum):
    EQUILATERAL = 'equilateral'
    SCALENE = 'scalene'
    ISOSCELES = 'isosceles'

def check_triangle(side1: int, side2: int, side3: int) -> tuple:
    if side1 + side2 < side3 or\
        side1 + side3 < side2 or\
            side2 + side3 < side1:
        raise InvalidTriangle
    return (side1, side2, side3)

def get_triangle_tipe(triangle: tuple) -> TriangleTipe:
    side1, side2, side3 = triangle
    if side1 == side2 and side2 == side3:
        return TriangleTipe.EQUILATERAL
    else:
        if side1 != side2 and side2 != side3 and side1 != side3:
            return TriangleTipe.SCALENE
        else:
            return TriangleTipe.ISOSCELES

def get_perimeter(triangle: tuple) -> float:
    return reduce(lambda a, b: a + b, triangle, 0)

def get_area(triangle: tuple) -> float:
    semi_perimeter = get_perimeter(triangle) / 2
    side1, side2, side3 = triangle
    pre_area = semi_perimeter * (semi_perimeter - side1) *\
        (semi_perimeter - side2) * (semi_perimeter - side3)
    return sqrt(pre_area)

def is_rectangle(triangle: tuple) -> bool:
    side1, side2, side3 = triangle
    _min = min(side1, side2, side3)
    _max = max(side1, side2, side3)
    _mid = sum(triangle) - _min - _max
    return _mid ** 2 + _min ** 2 == _max ** 2

def main():
    triangle = check_triangle(5, 4, 7)
    print(get_triangle_tipe(triangle))
    print(get_perimeter(triangle))
    print(get_area(triangle))

if __name__ == '__main__':
    main()
