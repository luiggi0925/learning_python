import pytest
import triangles

def test_check_triangle():
    assert triangles.check_triangle(10, 10, 10) == (10, 10, 10)

def test_check_triangle_invalid():
    with pytest.raises(triangles.InvalidTriangle):
        triangles.check_triangle(8, 11, 1)

def test_get_triangle_tipe_equilateral():
    assert triangles.get_triangle_tipe((10, 10, 10)) == triangles.TriangleTipe.EQUILATERAL

def test_get_triangle_tipe_isosceles():
    assert triangles.get_triangle_tipe((10, 12, 12)) == triangles.TriangleTipe.ISOSCELES

def test_get_triangle_tipe_scalene():
    assert triangles.get_triangle_tipe((10, 12, 14)) == triangles.TriangleTipe.SCALENE

def test_get_perimeter():
    assert triangles.get_perimeter((10, 11, 12)) == 33

def test_get_area():
    assert triangles.get_area((3, 4, 5)) == 6

def test_is_rectangle():
    assert triangles.is_rectangle((3, 4, 5))
